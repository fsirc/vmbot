# libvirtd + IRC bot

Yeah... what could go wrong? All of the IRC-related code is 100% written from scratch, and might be coupled to the project, unfortunately.

Code is somewhat messy, can't do anything about that (project deadlines), but it does not leak memory and should be bug-free.

You will need "hypervisor_daemon.py" running in the background on each hypervisor host, or the bot will be unable to assign IP addresses to VM MAC addresses (for DHCP). Modify config.example.json and rename it to config.json, and modify the VM template file as well.

## Build dependencies
- libcutil (https://gitlab.com/fsirc/libcutil)
- libgnutls - for TLS
- libjansson - for parsing the config file
- libsqlite3 - database
- libvirt - you know what this is for, surely?
- libxml - interacting with libvirtd

Adjust CFLAGS variable to set the dependency paths. Run `make && sudo make install`, to build and install the bot executable.

## Command usage

!vm is the command prefix for the bot.

Available commands:
- template
- deploy
- start
- stop
- ls (list VMs)

Example usage:
```
15:59 <@yyy> !vm ls
15:59 <+VMBot> yyy: #1 (nvm_ceuvymgbni) - created: Sun Aug 28 10:13:50 2022
15:59 <+VMBot> yyy: #2 (nvm_cvwrvfbwsd) - created: Sun Aug 28 15:03:28 2022
15:59 <@yyy> !vm destroy nvm_cvwrvfbwsd
15:59 <+VMBot> yyy: VM destroyed
15:59 <@yyy> !vm template
15:59 <+VMBot> yyy: available templates: [dev_chimaera, alpine_3.14.2]
```

## Production hypervisor node specifications
The hypervisor node that is being used to power the rizon/fsirc bot instance is a HP DL360 G6/G7 with the following specifications:
- CPU: Intel Xeon L5630
- Total RAM installed: 32GB
- Local storage: 2x 180GB Intel SSDs (RAID 1)
- iSCSI VM diskstore: 4x 1TB HDDs (RAID 10)
