CREATE TABLE IF NOT EXISTS vm (
    id integer primary key autoincrement,
    /* nvm_* */
    name varchar(30) not null,
    hypervisor_id varchar(20) not null,
    vm_uuid varchar(37) not null,
    owner varchar(31) not null,
    network varchar(60) not null,
    creation_date bigint not null
);
CREATE TABLE IF NOT EXISTS vm_ip (
    vm_id integer not null,
    ip_addr bigint not null unique,
    foreign key(vm_id) references vm(id)
);

