#include <sqlite3.h>
#include <libcutil/array.h>
#include "ip.h"
#ifndef H_DB
#define H_DB
struct db_vm_model {
    uint32_t id;
    char hypervisor_id[20+1];
    char vm_uuid[37+1];
    char vm_name[30+1];
    char network[60+1];
    char owner[31+1];
    uint64_t creation_date;
};
struct db_vm_ip_model {
    uint32_t vm_id;
    uint32_t ip_addr;
};
struct db_vm_ip_alloc {
    const char* ip_addr;
    const char* nat_pub_ip;
    uint16_t port_r_start;
    uint16_t port_r_end;
    uint16_t port_admin;
};
sqlite3* db_open();
array_t* db_lookup_vms_nick(sqlite3* dbc, const char* network, const char* nick);
int db_insert_vm(sqlite3* dbc, const struct db_vm_model* vm_model);
short db_insert_vm_ip(sqlite3* dbc, const struct db_vm_ip_model* ip_model);
short db_del_vm(sqlite3* dbc, uint32_t vm_id);
short db_del_vm_ip(sqlite3* dbc, uint32_t vm_id, uint32_t vm_ip);
array_t* db_lookup_vm_ips(sqlite3* dbc, uint32_t vm_id);
struct db_vm_model* db_lookup_vm(sqlite3* dbc, const char* vm_name);
short db_nick_owns_vm_by_name(sqlite3* dbc, const char* nick, const char* vm_name);
struct db_vm_ip_alloc* db_get_free_ip(sqlite3* dbc, const struct ip_range* irange, const char* hypervisor);
#endif
