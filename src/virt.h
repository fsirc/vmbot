#include <pthread.h>
#include <libcutil/table.h>
#include <libxml/parser.h>
#include <libvirt/libvirt.h>
#include "config.h"
#ifndef H_VIRT
#define H_VIRT
enum virt_qtid {
    VTQ_CREATE,
    VTQ_SS,
    VTQ_VNC,
    VTQ_DESTROY,
};
struct virt_dctx {
    char uri[200+1];
    char id[20+1];
    struct virt_conn* vc;
    struct virt_conns_ctx* vctx;
};
struct virt_conn {
    virConnectPtr vc;
    struct net_sock* daemon_conn;
    char* _id;
    int daemon_wid;
    struct virt_dctx* dctx;
};
struct virt_conns_ctx {
    table_t* hyp_conns;
};
struct vm_cresp {
    char* uuid;
    char* vm_name;
    char* hypervisor;
    virDomainPtr dom_ptr;
};
struct vm_creq {
    char* vm_name;
    uint32_t disk_mb;
    xmlDocPtr vm_tmpl;
    char* cdisk_img;
};
struct virt_vnc_info {
    char hyp_addr[50+1];
    char* pass;
    uint16_t port;
};
// ss => start/stop
struct vm_ss_req {
    char* vm_uuid;
    char* hypervisor_id;
    short start;
};
struct vm_destroy_req {
    char* vm_name;
    char* hypervisor_id;
};
struct vm_vnc_req {
    char* vm_name;
    char* hypervisor_id;
    char* pass;
};
struct virt_qtask {
    enum virt_qtid type;
    void* cb_arg;
    void*(*task_err_cb)(void*, void*);
    void*(*task_comp_cb)(void*, void*);
    struct vm_creq* creq;
    struct vm_ss_req* start_req;
    struct vm_destroy_req* destroy_req;
    struct vm_vnc_req* vnc_req;
    table_t* hypervisor_conns;
    struct vnc_ctx* vctx;
};
struct virt_evcb_ctx {
    pthread_mutex_t lock;
    short running;
};
struct virt_conns_ctx* virt_open_hyp_conns(table_t* hypervisors);
void virt_hypervisor_poll(struct virt_conns_ctx* hyp_ctx);
xmlDocPtr virt_open_template(char* filename);
const struct vm_cresp* virt_create_vm(table_t* hypervisor_conns, struct vm_creq* creq);
short virt_dhcp_assign_ip(struct virt_conn* vc, virDomainPtr vm, const char* ip_addr, short unset);
short virt_assign_pass_port(struct virt_conn* vc,
                            const char* vm_ip, const char* password, uint16_t admin_port);
short virt_ss_vm(table_t* hyp_conns, const char* hypervisor, const char* vm_uuid, const short start);
void virt_thread_worker(int* virt_pipe);
struct virt_qtask* virt_new_qtask();
void virt_cleanup(struct virt_conns_ctx* hyp_ctx);
#endif
