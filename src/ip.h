#include <stdint.h>
#ifndef H_IP
#define H_IP
// First port will be the SSHd port :0
#define IP_PORT_RANGE_CNT 31
struct ip_range {
    uint16_t prefix_len;
    uint32_t host_count;
    uint32_t network_addr;
    uint32_t broadcast_addr;
};
struct ip_range* ip_parse_range(char* range);
uint16_t ip_get_port_range(uint16_t base_port, uint16_t offset);
#endif
