#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <libcutil/array.h>
#include "net.h"
#include "util.h"
#include "services.h"

struct serv_ctx* serv_new_ctx(const char* serv_nick) {
    struct serv_ctx* s = malloc(sizeof(struct serv_ctx));
    s->stype = SERV_BACKEND_ANOPE;
    s->got_ctcp = 0;
    strncpy(s->nick, serv_nick, 30);
    return s;
}

void serv_init_conn(struct serv_ctx* ctx, struct net_sock* ns) {
    char* reqs[] = {"MACHINEGOD", "ABOUT", "VERSION", NULL}, **req = NULL;
    for(req = reqs; *req != NULL; req++) {
        net_sock_printf(ns, "PRIVMSG %s :\1%s\1\r\n", ctx->nick, *req);
    }
}

struct serv_status_resp* serv_handle_notice(struct serv_ctx* ctx,
        const char* from, 
        const char* msg) {
    struct serv_status_resp* r = NULL;
    char* tmp = NULL;
    array_t* msgp = NULL;
    if(strcmp(ctx->nick, from) != 0)
        return NULL;
    tmp = malloc(strlen(msg)+1);
    strcpy(tmp, msg);
    msgp = util_split_str(tmp, " ");
    if(strstr(msg, "\1VERSION")) {
        if(strstr(msg, "Atheme")) {
            ctx->got_ctcp = 1;
            ctx->stype = SERV_BACKEND_ATHEME;            
        }    
    } else if(strstr(msg, "findagrave")) {
        ctx->got_ctcp = 1;
        ctx->stype = SERV_BACKEND_ATHEME;
    } else if(strstr(msg, "\1ABOUT") && strstr(msg, "Atheme")) {
        ctx->got_ctcp = 1;
        ctx->stype = SERV_BACKEND_ATHEME;
    }
    if(((ctx->stype == SERV_BACKEND_ANOPE && msgp->len >= 3) || (ctx->stype == SERV_BACKEND_ATHEME && msgp->len >= 2)) && 
            (strcmp(msgp->items[0], "STATUS") == 0 || strcmp(msgp->items[1], "ACC") == 0)) {
        r = malloc(sizeof(struct serv_status_resp));
        r->registered = atoi(msgp->items[2]) == 3 ? 1 : 0;
        strncpy(r->nick, ctx->stype == SERV_BACKEND_ANOPE ? msgp->items[1] : msgp->items[0], 30);
    } 
    array_free(msgp, 0);
    free(tmp);
    return r;
}

char* serv_build_info_cmd(const struct serv_ctx* ctx, const char* nick) {
    const char* fcmd = NULL;
    char* cmd = NULL;
    switch(ctx->stype) {
        case SERV_BACKEND_ANOPE:
            fcmd = "PRIVMSG %s :status %s\r\n";
            break;
        case SERV_BACKEND_ATHEME:
            fcmd = "PRIVMSG %s :acc %s\r\n";
            break;
    }
    cmd = malloc(250+1);
    snprintf(cmd, 250+1, fcmd, ctx->nick, nick);
    return cmd;
}
