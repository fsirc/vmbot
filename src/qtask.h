#include "msg_handler.h"
#ifndef H_QTASK
#define H_QTASK
void qtask_cb_virt_gerr(void* c, struct msgh_cb_ctx* ctx);
void qtask_cb_virt_ss_ok(void* c, struct msgh_cb_ctx* ctx);
void qtask_cb_virt_destroy(void* c, struct msgh_cb_ctx* ctx);
void qtask_cb_vnc_ok(void* c, struct msgh_cb_ctx* ctx);
#endif
