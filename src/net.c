#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <poll.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <netdb.h>
#include <stdarg.h>
#include <assert.h>
#ifdef __FreeBSD__
#include <netinet/in.h>
#endif
#include "net.h"

struct net_sock* net_sock_connect(const char* hostname, uint16_t port, int net_opts) {
    int fd = -1, n;
    short nonblock = (net_opts & NETO_NONBLOCK);
    socklen_t opt_len = sizeof(int);
    short connected = 0;
    struct addrinfo* ai, hints = {0}, *rp;
    struct net_sock* ns = NULL;
    struct pollfd pfd;
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_ADDRCONFIG, hints.ai_protocol = 0;
    if(getaddrinfo(hostname, NULL, &hints, &ai) != 0)
        return NULL;
    ns = malloc(sizeof(struct net_sock));
    ns->tls_enabled = 0;
    if(net_opts & NETO_TLS_ENABLE) {
        ns->tls_enabled = 1;
        gnutls_certificate_allocate_credentials(&ns->tls_xcred);
        gnutls_certificate_set_x509_system_trust(ns->tls_xcred);
        gnutls_init(&ns->tls_ses, GNUTLS_CLIENT|GNUTLS_NONBLOCK);
        gnutls_credentials_set(ns->tls_ses, GNUTLS_CRD_CERTIFICATE, ns->tls_xcred);
        gnutls_session_set_verify_cert(ns->tls_ses, hostname, 0);
        gnutls_set_default_priority(ns->tls_ses);
        gnutls_handshake_set_timeout(ns->tls_ses, 5*1000);
    }

    for(rp = ai; rp != NULL; rp = rp->ai_next) {
        // let's try 10s per server...
        fd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if(fd == -1)
            continue;
        ns->fd = fd;
        if(rp->ai_family == AF_INET)
            ((struct sockaddr_in*)rp->ai_addr)->sin_port = htons(port);
        else
            ((struct sockaddr_in6*)rp->ai_addr)->sin6_port = htons(port);
        fcntl(fd, F_SETFL, O_NONBLOCK);
        if(connect(fd, rp->ai_addr, rp->ai_addrlen) == -1 && errno != EINPROGRESS) {
            close(fd);
            continue;
        }
        if(net_opts & NETO_TLS_ENABLE)
            gnutls_transport_set_int(ns->tls_ses, fd);
        connected = nonblock ? 1 : 0;
        if(!nonblock) {
            pfd.fd = fd, pfd.events = POLLOUT;
            if(poll(&pfd, 1, 5*1000) < 1)
                continue;
            // additional validation, of course...
            if(getsockopt(fd, SOL_SOCKET, SO_ERROR, &n, &opt_len) != 0 || n != 0)
                continue;
            connected = 1;
        }
    }
    if(net_opts & NETO_TLS_ENABLE) {
        if(!(net_opts & NETO_TLS_VERIFY))
            gnutls_session_set_verify_cert(ns->tls_ses, NULL, 0);
        fcntl(fd, F_SETFL, 0);
        n = gnutls_handshake(ns->tls_ses);
        fcntl(fd, F_SETFL, O_NONBLOCK);
    }
    if(!connected || ((net_opts & NETO_TLS_ENABLE) && n < 0)) {
        close(fd);
        free(ns);
        ns = NULL;
    }
    freeaddrinfo(ai);
    return ns;
}

void net_sock_close(struct net_sock* ns) {
    assert(ns != NULL);
    close(ns->fd);
    if(ns->tls_enabled) {
        gnutls_certificate_free_credentials(ns->tls_xcred);
        gnutls_deinit(ns->tls_ses);
    }
    free(ns);
}

int net_sock_printf(struct net_sock* ns, char* fmt, ...) {
    char buf[1024] = {0};
    int c = -1;
    va_list args;
    va_start(args, fmt);
    c = vsnprintf(buf, 1024, fmt, args);
    va_end(args);
    return net_sock_write(ns, buf, c);
}

int net_sock_write(struct net_sock* ns, void* data, int len) {
    if(ns->tls_enabled)
        return gnutls_record_send(ns->tls_ses, data, len);
    return send(ns->fd, data, len, 0);
}

int net_sock_recv(struct net_sock* ns, void* data, int len) {
    int c = -1;
    if(ns->tls_enabled) {
        c = gnutls_record_recv(ns->tls_ses, data, len);
        if(c == GNUTLS_E_AGAIN)
            errno = EAGAIN;
    } else
        c = recv(ns->fd, data, len, 0);
    return c;
}
