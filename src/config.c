#include <jansson.h>
#include <libcutil/array.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "ip.h"
#include "util.h"
#include "config.h"

struct config* CONFIG;

#define ERR_CODE_MEM "failed to allocate memory"

enum config_opt_type {
    COPT_STR = 0,
    COPT_INT = 1,
    COPT_BOOL = 2,
    COPT_ARR = 3,
    COPT_TBL = 4
};
struct config_opt {
    const char* name;
    enum config_opt_type type;
    short arr_val_type;
    short required;
};

array_t* _jarr_str_to_arr(json_t* jarr) {
    array_t* arr = array_new();
    json_t* j;
    char* s;
    for(int i = 0; i < json_array_size(jarr); i++) {
        j = json_array_get(jarr, i);
        s = malloc(json_string_length(j)+1);
        strcpy(s, json_string_value(j));
        array_insert(arr, s);
    }
    return arr;
}

static short config_validate_opts(json_t* jdata, struct config_opt* opts) {
    json_t* copt = NULL;
    struct config_opt *co;
    for(co = opts; co->name != NULL; co++) {
        copt = json_object_get(jdata, co->name);
        if(co->required && !copt) {
            return 0;
        } else if(!co->required && !copt)
            continue;
        switch(co->type) {
        case COPT_BOOL:
            if(!json_is_boolean(copt))
                return 0;
            break;
        case COPT_INT:
            if(!json_is_integer(copt))
                return 0;
            break;
        case COPT_STR:
            if(!json_is_string(copt))
                return 0;
            break;
        case COPT_ARR:
            if(!json_is_array(copt))
                return 0;
            break;
        case COPT_TBL:
            if(!json_is_object(copt))
                return 0;
            break;
        }
    }
    return 1;
}

static array_t* config_parse_hyp_subnets(json_t* subnets) {
    array_t* s = array_new();
    struct ip_range* ir = NULL;
    const char* k = NULL;
    int i = -1;
    for(i = 0; i < json_array_size(subnets); i++) {
        if(!(k = json_string_value(json_array_get(subnets, i))))
            continue;
        if(!(ir = ip_parse_range((char*)k)))
            continue;
        array_insert(s, ir);
    }
    return s;
}

static short config_parse_nat_section(json_t* nat_sec) {
    int i = -1;
    const char* k = NULL;
    char* nat_pub_ip = NULL;
    struct ip_range* ir = NULL;
    table_t* hyp_ranges = NULL;
    json_t* jval = NULL, *j = NULL;
    CONFIG->nat.enabled = json_boolean_value(json_object_get(nat_sec, "enabled"));
    CONFIG->nat.start_port = json_integer_value(json_object_get(nat_sec, "start_port"));
    CONFIG->nat.end_port = json_integer_value(json_object_get(nat_sec, "end_port"));
    if(!(j = json_object_get(nat_sec, "ranges")) || !json_is_array(j))
        return 0;
    for(i = 0; i < json_array_size(j); i++) {
        jval = json_array_get(j, i);
        if(json_array_size(jval) < 3)
            continue;
        if(!(k = json_string_value(json_array_get(jval, 0))) ||
                !table_lookup_key(CONFIG->hypervisor, k))
            continue;
        if(!(ir = ip_parse_range((char*)json_string_value(
                                     json_array_get(jval, 1)))))
            continue;
        nat_pub_ip = strdup(json_string_value(json_array_get(jval, 2)));
        hyp_ranges = table_lookup_key(CONFIG->nat.ranges, k);
        table_insert(hyp_ranges, &ir->network_addr, nat_pub_ip);
        free(ir);
    }
    return 1;
}

static short config_parse_vnc_gw_section(json_t* vnc_gw_sec) {
    json_t* j = NULL;
    CONFIG->vnc_gw.external_port = 0;
    CONFIG->vnc_gw.enabled = json_boolean_value(json_object_get(vnc_gw_sec,
                             "enabled"));
    if(!(j = json_object_get(vnc_gw_sec, "addr")) || !json_is_string(j))
        return 0;
    strncpy(CONFIG->vnc_gw.addr, json_string_value(j),
            sizeof(CONFIG->vnc_gw.addr));
    strncpy(CONFIG->vnc_gw.external_addr, json_string_value(j),
            sizeof(CONFIG->vnc_gw.external_addr));
    if((j = json_object_get(vnc_gw_sec, "external_addr")) && json_is_string(j)) {
        strncpy(CONFIG->vnc_gw.external_addr, json_string_value(j),
                sizeof(CONFIG->vnc_gw.external_addr));
    }
    if((j = json_object_get(vnc_gw_sec, "external_port")) && json_is_integer(j))
        CONFIG->vnc_gw.external_port = json_integer_value(j);
    if(!(j = json_object_get(vnc_gw_sec, "port")) || !json_is_integer(j))
        return 0;
    CONFIG->vnc_gw.port = json_integer_value(j);
    if(!(j = json_object_get(vnc_gw_sec, "pass")) || !json_is_string(j))
        return 0;
    strncpy(CONFIG->vnc_gw.pass, json_string_value(j),
            sizeof(CONFIG->vnc_gw.pass));
    return 1;
}

void config_read() {
    json_t* cfg_data = NULL, *irc_data = NULL, *hyp_data = NULL,
            *nat_data = NULL, *logger_data = NULL, *vnc_gw_data = NULL,
             *t = NULL;
    struct config_hypervisor_tmpl* hyp_tmpl = NULL;
    struct config_hypervisor *ch = NULL;
    struct config_irc* ic = NULL;
    json_t* jval = NULL, *j_iter = NULL, *j = NULL;
    int i = -1;
    char* err = NULL;
    const char* k = NULL;
    struct config_opt cfg_irc_opts[] = {
        {"port", COPT_INT, 1},
        {"tls", COPT_BOOL, 1},
        {"host", COPT_STR, 1},
        {"nick", COPT_STR, 1},
        {"username", COPT_STR, 1},
        {"realname", COPT_STR, 1},
        {"services_validation", COPT_BOOL, 0},
        {"services_nick", COPT_STR, 0},
        {"auth", COPT_TBL, 0}, {NULL}
    }, cfg_vnc_gw_opts[] = {
        {"addr", COPT_STR, 0},
        {"port", COPT_INT, 0},
        {"pass", COPT_STR, 0},
        {"enabled", COPT_BOOL, 1}, {NULL}
    }, cfg_opts[] = {
        {"irc", COPT_TBL, 1},
        {"nat", COPT_TBL, 1},
        {"logger", COPT_TBL, 0},
        {"vnc_gw", COPT_TBL, 1},
        {"templates", COPT_ARR, 1},
        {"template_format", COPT_STR, 1},
        {"tls_verify", COPT_BOOL, 1},
        {"vm_user_limit", COPT_INT, 1},
        {"vm_disk_mb", COPT_INT, 1},
        {NULL}
    }, cfg_nat_opts[] = {
        {"enabled", COPT_BOOL, 1},
        {"start_port", COPT_INT, 1},
        {"end_port", COPT_INT, 1},
        {"ranges", COPT_ARR, 1}, {NULL}
    }, cfg_hyp_opts[] = {
        {"id", COPT_STR, 1},
        {"uri", COPT_STR, 1},
        {"daemon_addr", COPT_STR, 1},
        {"vnc_ln_addr", COPT_STR, 0},
        {"location", COPT_STR, 1},
        {"network", COPT_STR, 1},
        {"diskstore", COPT_STR, 1},
        {"keepalive", COPT_BOOL, 0},
        {"subnets", COPT_ARR, 1},
        {"templates", COPT_TBL, 1},
        {NULL}
    }, cfg_auth_opts[] = {{"method", COPT_STR, 1}, {"wait", COPT_BOOL, 1}, {"username", COPT_STR, 1}, {"password", COPT_STR, 1}, {NULL}}, cfg_logger_opts[] = {{"file_path", COPT_STR, 1}, {"stdout", COPT_BOOL, 0}, {NULL}};

    if(!(CONFIG = malloc(sizeof(struct config))) ||
            !(CONFIG->irc = table_new()) ||
            !(CONFIG->hypervisor = table_new())) {
        err = ERR_CODE_MEM;
        goto ret;
    }
    CONFIG->nat.ranges = table_new();
    CONFIG->templates = NULL;
    memset(CONFIG->template_format, 0, 5);
    memset(&CONFIG->logger, 0, sizeof(CONFIG->logger));
    if(!(cfg_data = json_load_file("config.json", 0, NULL)) ||
            !(irc_data = json_object_get(cfg_data, "irc")) ||
            !(hyp_data = json_object_get(cfg_data, "hypervisor")) ||
            !(nat_data = json_object_get(cfg_data, "nat")) ||
            !(logger_data = json_object_get(cfg_data, "logger")) ||
            !(vnc_gw_data = json_object_get(cfg_data, "vnc_gw"))) {
        err = "failed to open/parse config file";
        goto ret;
    }
    if(!config_validate_opts(cfg_data, cfg_opts) || !config_validate_opts(nat_data, cfg_nat_opts)
            || !config_validate_opts(logger_data, cfg_logger_opts)
            || !config_validate_opts(vnc_gw_data, cfg_vnc_gw_opts)) {
        err = "failed to parse config file";
        goto ret;
    }
    CONFIG->logger.log_stdout = json_boolean_value(json_object_get(logger_data, "stdout"));
    strncpy(CONFIG->logger.dst_filepath, json_string_value(json_object_get(logger_data, "file_path")),
            sizeof(CONFIG->logger.dst_filepath));
    CONFIG->tls_verify = json_boolean_value(json_object_get(cfg_data, "tls_verify"));
    CONFIG->vm_user_limit = json_integer_value(json_object_get(cfg_data, "vm_user_limit"));
    CONFIG->vm_disk_mb = json_integer_value(json_object_get(cfg_data, "vm_disk_mb"));
    strncpy(CONFIG->template_format, json_string_value(json_object_get(cfg_data, "template_format")),
            sizeof(CONFIG->template_format));
    CONFIG->templates = _jarr_str_to_arr(json_object_get(cfg_data, "templates"));
    json_object_foreach(irc_data, k, jval) {
        if(!config_validate_opts(jval, cfg_irc_opts)) {
            err = "failed to parse irc section";
            goto ret;
        }
        ic = malloc(sizeof(struct config_irc));
        memset(&ic->auth, 0, sizeof(ic->auth));
        memset(&ic->pass, 0, sizeof(ic->pass));
        ic->tls_enabled = json_boolean_value(json_object_get(jval, "tls"));
        ic->services_validation = json_boolean_value(json_object_get(jval, "services_validation"));
        if((j = json_object_get(jval, "auth"))) {
            if(!config_validate_opts(j, cfg_auth_opts)) {
                err = "failed to parse irc -> auth section";
                goto ret;
            }
            ic->auth.wait = json_boolean_value(json_object_get(j, "wait"));
            ic->auth.method = strcmp(json_string_value(json_object_get(j, "method")), "nickserv") == 0
                              ? AUTH_METHOD_NICKSERV : -1;
            strncpy(ic->auth.username, json_string_value(json_object_get(j, "username")), sizeof(ic->auth.username));
            strncpy(ic->auth.password, json_string_value(json_object_get(j, "password")), sizeof(ic->auth.password));
        }
        ic->on_connect_cmds = NULL;
        strcpy(ic->services_nick, "NickServ");
        if((t = json_object_get(jval, "services_nick")))
            strncpy(ic->services_nick, json_string_value(t), sizeof(ic->services_nick));
        strncpy(ic->name, k, sizeof(ic->name));
        strncpy(ic->nick, json_string_value(json_object_get(jval, "nick")), sizeof(ic->nick));
        strncpy(ic->host, json_string_value(json_object_get(jval, "host")), sizeof(ic->host));
        strncpy(ic->username, json_string_value(json_object_get(jval, "username")), sizeof(ic->username));
        strncpy(ic->realname, json_string_value(json_object_get(jval, "realname")), sizeof(ic->realname));
        ic->port = json_integer_value(json_object_get(jval, "port"));
        if((t = json_object_get(jval, "pass")))
            strncpy(ic->pass, json_string_value(t), sizeof(ic->pass));
        if((t = json_object_get(jval, "on_connect_cmds")))
            ic->on_connect_cmds = strdup(json_string_value(t));
        ic->channels = _jarr_str_to_arr(json_object_get(jval, "channels"));
        table_insert(CONFIG->irc, k, ic);
    }
    for(i = 0; i < json_array_size(hyp_data); i++) {
        jval = json_array_get(hyp_data, i);
        if(!config_validate_opts(jval, cfg_hyp_opts)) {
            err = "failed to parse hypervisor section";
            goto ret;
        }
        ch = malloc(sizeof(struct config_hypervisor));
        ch->keepalive = json_integer_value(json_object_get(jval, "keepalive"));
        ch->templates = table_new();
        strncpy(ch->id, json_string_value(json_object_get(jval, "id")), sizeof(ch->id));
        strncpy(ch->diskstore, json_string_value(json_object_get(jval, "diskstore")),
                sizeof(ch->diskstore));
        strncpy(ch->network, json_string_value(json_object_get(jval, "network")),
                sizeof(ch->network));
        strncpy(ch->location, json_string_value(json_object_get(jval, "location")),
                sizeof(ch->location));
        strncpy(ch->uri, json_string_value(json_object_get(jval, "uri")),
                sizeof(ch->uri));
        strncpy(ch->daemon_addr, json_string_value(json_object_get(jval, "daemon_addr")),
                sizeof(ch->daemon_addr));
        if((j = json_object_get(jval, "vnc_ln_addr")))
            strncpy(ch->vnc_ln_addr, json_string_value(j),
                    sizeof(ch->vnc_ln_addr));
        ch->subnets = config_parse_hyp_subnets(json_object_get(jval, "subnets"));
        jval = json_object_get(jval, "templates");
        for(j_iter = json_object_iter(jval); j_iter;
                j_iter = json_object_iter_next(jval, j_iter)) {
            j = json_object_iter_value(j_iter);
            if(!json_is_array(j) || json_array_size(j) < 2)
                continue;
            hyp_tmpl = malloc(sizeof(struct config_hypervisor_tmpl));
            strncpy(hyp_tmpl->store_pool, json_string_value(json_array_get(j, 0)),
                    sizeof(hyp_tmpl->store_pool));
            strncpy(hyp_tmpl->img_filename, json_string_value(json_array_get(j, 1)),
                    sizeof(hyp_tmpl->img_filename));
            table_insert(ch->templates, json_object_iter_key(j_iter), hyp_tmpl);
        }
        table_insert(CONFIG->hypervisor, ch->id, ch);
        table_insert(CONFIG->nat.ranges, ch->id, table_new());
    }
    if(!config_parse_nat_section(nat_data)) {
        err = "failed to parse nat section";
        goto ret;
    }
    if(json_boolean_value(json_object_get(vnc_gw_data, "enabled"))) {
        if(!config_parse_vnc_gw_section(vnc_gw_data)) {
            err = "failed to parse vnc_gw section";
            goto ret;
        }
    }
ret:
    if(err != NULL) {
        if(CONFIG->hypervisor) table_free(CONFIG->hypervisor, 0);
        if(CONFIG->irc) table_free(CONFIG->irc, 0);
        if(CONFIG) free(CONFIG);
        fprintf(stderr, "failed to read config: %s\n", err);
        exit(1);
    }
    json_decref(cfg_data);
}

void config_free() {
    int i = -1;
    void* f = NULL;
    for(i = 0; i < CONFIG->irc->len; i++) {
        f = table_lookup_index(CONFIG->irc, i);
        array_free(((struct config_irc*)f)->channels, 1);
    }
    table_free(CONFIG->irc, 1);
    for(i = 0; i < CONFIG->hypervisor->len; i++) {
        f = table_lookup_index(CONFIG->hypervisor, i);
        table_free(((struct config_hypervisor*)f)->templates, 1);
        array_free(((struct config_hypervisor*)f)->subnets, 1);
    }
    for(i = 0; i < CONFIG->nat.ranges->len; i++) {
        f = table_lookup_index(CONFIG->nat.ranges, i);
        table_free(f, 1);
    }
    array_free(CONFIG->templates, 1);
    table_free(CONFIG->nat.ranges, 0);
    table_free(CONFIG->hypervisor, 1);
    free(CONFIG);
}
