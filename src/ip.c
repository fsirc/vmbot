#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <assert.h>
#include "config.h"
#include "ip.h"

static int ip_bitmask(uint16_t prefix_len) {
    assert(prefix_len < 32);
    return ~(~0U >> prefix_len);
}

// CIDR format, so 10.0.0.0/24, as an example
// IPv6 is NOT supported
struct ip_range* ip_parse_range(char* range) {
    char* s, b[18+1] = {0};
    uint16_t plen = 0;
    uint32_t ip_addr = 0;
    struct ip_range* r = NULL;
    if(!(s = strstr(range, "/"))) return NULL;
    s++;
    if((plen = atoi(s)) < 8) return NULL;
    strncpy(b, range, strlen(range)-3);
    ip_addr = ntohl(inet_addr(b));
    r = malloc(sizeof(struct ip_range));
    r->prefix_len = plen;
    r->host_count = ~ip_bitmask(plen);
    r->network_addr = ip_addr;
    r->broadcast_addr = ip_addr|~ip_bitmask(plen);
    return r;
}

uint16_t ip_get_port_range(uint16_t base_port, uint16_t offset) {
    uint32_t c = (base_port+(IP_PORT_RANGE_CNT*offset));
    return c < 65536 ? c : 0;
}
