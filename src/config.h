#include <libcutil/array.h>
#include <libcutil/table.h>
#ifndef H_CONFIG
#define H_CONFIG
enum config_auth_method {
    AUTH_METHOD_SASL = 0,
    AUTH_METHOD_NICKSERV = 1
};
struct config_irc_auth {
    enum config_auth_method method;
    short wait;
    char username[30+1];
    char password[60+1];
};
struct config_irc {
    char name[60+1];
    char host[50+1];
    char pass[60+1];
    uint16_t port;
    char nick[31];
    char services_nick[31];
    char username[20+1];
    char realname[30+1];
    struct config_irc_auth auth;
    char* on_connect_cmds;
    short services_validation;
    short tls_enabled;
    array_t* channels;
};
// Network Address Translation, more specifically DNAT (destination)
struct config_nat {
    short enabled;
    uint16_t start_port;
    uint16_t end_port;
    // ranges to apply DNAT to
    // hypervisor_id:[range_str -> public_ip]
    table_t* ranges;
};
struct config_vnc_gw {
    char addr[50+1];
    char external_addr[100+1];
    uint16_t external_port;
    uint16_t port;
    char pass[50+1];
    short enabled;
};
struct config_hypervisor {
    char id[20+1];
    char uri[200+1];
    char daemon_addr[50+1];
    char vnc_ln_addr[50+1];
    char location[2+1];
    char network[20+1];
    char diskstore[20+1];
    unsigned short keepalive;
    array_t* subnets;
    table_t* templates;
};
struct config_hypervisor_tmpl {
    char store_pool[50+1];
    char img_filename[50+1];
};
struct config_logger {
    char dst_filepath[500+1];
    short log_stdout;
};
struct config {
    table_t* irc;
    struct config_nat nat;
    struct config_vnc_gw vnc_gw;
    struct config_logger logger;
    table_t* hypervisor;
    array_t* templates;
    char template_format[5+1];
    uint16_t vm_user_limit;
    uint32_t vm_disk_mb;
    short tls_verify;
};
extern struct config* CONFIG;
void config_read();
void config_free();
#endif
