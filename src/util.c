#include <libcutil/array.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <time.h>
#include "util.h"

array_t* util_split_str(char* str, char* delim) {
    char* sp, *s, *f;
    array_t* arr = array_new();
    for(f = str; ; f = NULL) {
        s = strtok_r(f, delim, &sp);
        if(!s)
            break;
        array_insert(arr, s);
    }
    return arr;
}

// If lowercase is set to true, then we'll include uppercase and lowercase characters
short util_gen_rand_str(char* buf, uint16_t len, unsigned short lowercase) {
    char c[8];
    int i = 0, b = 0, fd = -1;
    if((fd = open("/dev/urandom", O_RDONLY)) < 1)
        return 0;
    while(i < len) {
        if(b < 1 && (b = read(fd, c, 8)) < 1)
            break;
        b--;
//      thanks, Shahid
        if((!lowercase ? (unsigned)c[b]|0x20 : c[b]) - 'a' > 'z'-'a')
            continue;
        buf[i] = c[b];
        i++;
    }
    close(fd);
    if(i < len)
        return 0;
    buf[i++] = '\0';
    return 1;
}
