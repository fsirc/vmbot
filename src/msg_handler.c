#include <stdio.h>
#include <string.h>
#include <libvirt/libvirt.h>
#include <libvirt/virterror.h>
#include <time.h>
#include <unistd.h>
#include <arpa/inet.h>
#include "irc.h"
#include "util.h"
#include "db.h"
#include "logger.h"
#include "config.h"
#include "virt.h"
#include "vm.h"
#include "qtask.h"
#include "msg_handler.h"

#define msgh_lookup_vm_cmp if(!(r = db_lookup_vm(ctx->dbc, vm_name)) \
        || strcmp(r->owner, im->from) != 0 \
        || strcmp(r->network, ctx->irc_cfg->name) != 0) { \
    if(r) free(r); return; \
}

// Only used for callbacks
static struct msgh_cb_ctx* _new_msgh_cb_ctx(struct irc_ctx* ctx, struct irc_privmsg* im) {
    struct msgh_cb_ctx* cb_ctx = NULL;
    cb_ctx = malloc(sizeof(struct msgh_cb_ctx));
    cb_ctx->dbc = ctx->dbc;
    cb_ctx->hyp_conns = ctx->hyp_conns;
    cb_ctx->vctx = ctx->vctx;
    cb_ctx->pending_deployments = ctx->pending_deployments;
    cb_ctx->from_nick = strdup(im->from);
    cb_ctx->network = strdup(ctx->irc_cfg->name);
    cb_ctx->channel = strdup(im->channel);
    cb_ctx->ns = ctx->ns;
    return cb_ctx;
}

static void _cb_virt_vm_creation(struct vm_cresp* cresp, struct msgh_cb_ctx* ctx) {
    struct db_vm_model vm_entry = {0};
    struct virt_conn* vc = NULL;
    struct config_hypervisor* hyp = NULL;
    struct db_vm_ip_model ip_model;
    struct db_vm_ip_alloc* ip_alloc = NULL;
    char buf[IRC_MSG_LEN], password[VM_PASSWORD_LEN+1], *err = NULL;
    int i = -1;
    table_del(ctx->pending_deployments, ctx->from_nick);
    if(!(vc = table_lookup_key(ctx->hyp_conns, cresp->hypervisor)))
        return;
    if(virDomainCreate(cresp->dom_ptr) < 0 || virDomainSetAutostart(cresp->dom_ptr, 1)) {
        logger_log(LOG_LEVEL_ERR, "msgh", "failed to start VM/set autostart for '%s' (owner: '%s' @ %s) on '%s'",
                   cresp->uuid, ctx->from_nick, ctx->network, cresp->hypervisor);
        snprintf(buf, sizeof(buf), "%s: failed to start VM, contact an admin", ctx->from_nick);
        irc_send_msg(ctx->ns, ctx->channel, buf);
        virDomainFree(cresp->dom_ptr);
        return;
    }
    if(!(hyp = table_lookup_key(CONFIG->hypervisor, cresp->hypervisor))) {
        err = "failed to lookup hypervisor cfg";
        goto ret_free;
    }
    for(i = 0; i < hyp->subnets->len; i++) {
        if((ip_alloc = db_get_free_ip(ctx->dbc, hyp->subnets->items[i], cresp->hypervisor))) {
            break;
        }
    }
    if(!ip_alloc) {
        err = "failed to allocate IP address";
        goto ret_free;
    }
    ip_model.ip_addr = ntohl(inet_addr(ip_alloc->ip_addr));
    virt_dhcp_assign_ip(vc, cresp->dom_ptr, ip_alloc->ip_addr, 0);
    virDomainFree(cresp->dom_ptr);
    strncpy(vm_entry.vm_uuid, cresp->uuid, sizeof(vm_entry.vm_uuid));
    strncpy(vm_entry.vm_name, cresp->vm_name, sizeof(vm_entry.vm_name));
    strncpy(vm_entry.hypervisor_id, cresp->hypervisor, sizeof(vm_entry.hypervisor_id));
    strncpy(vm_entry.owner, ctx->from_nick, sizeof(vm_entry.owner));
    strncpy(vm_entry.network, ctx->network, sizeof(vm_entry.network));
    if((i = db_insert_vm(ctx->dbc, &vm_entry)) && i >= 0) {
        ip_model.vm_id = i;
        db_insert_vm_ip(ctx->dbc, &ip_model);
        util_gen_rand_str(password, VM_PASSWORD_LEN, 0);
        if(ip_alloc->nat_pub_ip)
            snprintf(buf, sizeof(buf), "VM IP: %s (external: %s), password is \"%s\", admin/SSH port is %d, port range is %d:%d", ip_alloc->ip_addr, ip_alloc->nat_pub_ip,
                     password, ip_alloc->port_admin, ip_alloc->port_r_start, ip_alloc->port_r_end);
        else
            snprintf(buf, sizeof(buf), "VM IP: %s, password is \"%s\"", ip_alloc->ip_addr, password);
        virt_assign_pass_port(vc, ip_alloc->ip_addr, password, ip_alloc->port_admin);
        irc_send_msg(ctx->ns, ctx->from_nick, buf);
        memset(password, 0, sizeof(password));
        logger_log(LOG_LEVEL_INFO, "msgh", "VM deployed for nick '%s' (%s: %s/%s)", ctx->from_nick, ctx->network,
                   cresp->uuid, ip_alloc->ip_addr, cresp->vm_name);
        snprintf(buf, sizeof(buf),
                 "%s: VM deployed, check your PMs for more information", ctx->from_nick);
        irc_send_msg(ctx->ns, ctx->channel, buf);
    } else {
        logger_log(LOG_LEVEL_INFO, "msgh", "failed to insert DB entry for created VM, for nick '%s' @ '%s' (%s)", ctx->from_nick, cresp->hypervisor, cresp->uuid);
        snprintf(buf, sizeof(buf),
                 "%s: DB error occurred, contact an admin", ctx->from_nick);
        irc_send_msg(ctx->ns, ctx->channel, buf);
    }
ret_free:
    if(err != NULL) {
        logger_log(LOG_LEVEL_ERR, "msgh", "failed to deploy VM, reason (triggered by '%s' @ %s): %s", err, ctx->from_nick, ctx->network);
        snprintf(buf, sizeof(buf),
                 "%s: an error occurred, contact an admin", ctx->from_nick);
        irc_send_msg(ctx->ns, ctx->channel, buf);
    }
    if(ip_alloc) free(ip_alloc);
    free(ctx->from_nick);
    free(ctx->channel);
    free(ctx);
    free(cresp->uuid);
    free(cresp->vm_name);
    free(cresp);
}

void _cb_vm_ls(struct irc_ctx* ctx, struct irc_privmsg* im) {
    int i;
    char buf[IRC_MSG_LEN];
    virDomainPtr vm_obj = NULL;
    struct virt_conn* vc = NULL;
    struct db_vm_model* row;
    array_t* rows = db_lookup_vms_nick(ctx->dbc, ctx->irc_cfg->name, im->from);
    if(!rows) {
        snprintf(buf, sizeof(buf),
                 "%s: you do not have any VMs currently", im->from);
        return irc_send_msg(ctx->ns, im->channel, buf);
    }
    for(i = 0; i < rows->len; i++) {
        row = rows->items[i];
        if(!(vc = table_lookup_key(ctx->hyp_conns, row->hypervisor_id))) {
            logger_log(LOG_LEVEL_ERR, "failed to lookup hypervisor node connection for VM '%s', for user '%s'",
                       row->vm_uuid, im->from);
            snprintf(buf, sizeof(buf),
                     "%s: failed to query hypervisor(s), contact an admin for assistance", im->from);
            irc_send_msg(ctx->ns, im->channel, buf);
            break;
        }
        if(!(vm_obj = virDomainLookupByUUIDString(vc->vc, row->vm_uuid))
                || virGetLastErrorCode() == VIR_ERR_NO_DOMAIN) {
            snprintf(buf, sizeof(buf),
                     "%s: failed to query one or more VM(s)", im->from);
            irc_send_msg(ctx->ns, im->channel, buf);
            break;
        }
        snprintf(buf, sizeof(buf),
                 "%s: #%d (%s) - created: %s", im->from, i+1, row->vm_name,
                 asctime(gmtime((const time_t*)&row->creation_date)));
        irc_send_msg(ctx->ns, im->channel, buf);
        virDomainFree(vm_obj);
    }
    array_free(rows, 1);
}

static void _cb_vm_ss(struct irc_ctx* ctx,
                      struct irc_privmsg* im,
                      const char* vm_name,
                      unsigned short start) {
    struct virt_qtask qtask;
    struct db_vm_model* r = NULL;
    struct vm_ss_req* ss_req = NULL;
    msgh_lookup_vm_cmp
    ss_req = malloc(sizeof(struct vm_ss_req));
    ss_req->vm_uuid = strdup(r->vm_uuid);
    ss_req->hypervisor_id = strdup(r->hypervisor_id);
    ss_req->start = start;
    qtask.type = VTQ_SS;
    qtask.start_req = ss_req;
    qtask.task_comp_cb = (void*)qtask_cb_virt_ss_ok;
    qtask.task_err_cb = (void*)qtask_cb_virt_gerr;
    qtask.hypervisor_conns = ctx->hyp_conns;
    qtask.cb_arg = _new_msgh_cb_ctx(ctx, im);
    write(ctx->virt_pipe, &qtask, sizeof(struct virt_qtask));
    free(r);
}

static void _cb_vm_destroy(struct irc_ctx* ctx,
                           struct irc_privmsg* im,
                           const char* vm_name) {
    struct virt_qtask qtask = {0};
    struct db_vm_model* r = NULL;
    struct vm_destroy_req* dr = NULL;
    struct virt_conn* vc = NULL;
    struct in_addr ia;
    virDomainPtr dom_ptr = NULL;
    array_t* vm_ips = NULL;
    int i = -1;
    uint32_t ip_addr = 0;
    msgh_lookup_vm_cmp
    if(!(vc = table_lookup_key(ctx->hyp_conns, r->hypervisor_id)))
        return;
    if(!(dom_ptr = virDomainLookupByUUIDString(vc->vc, r->vm_uuid)))
        return;
    dr = malloc(sizeof(struct vm_destroy_req));
    dr->hypervisor_id = strdup(r->hypervisor_id);
    dr->vm_name = strdup(r->vm_name);
    qtask.type = VTQ_DESTROY;
    qtask.task_comp_cb = (void*)qtask_cb_virt_destroy;
    qtask.task_err_cb = (void*)qtask_cb_virt_gerr;
    qtask.hypervisor_conns = ctx->hyp_conns;
    qtask.cb_arg = _new_msgh_cb_ctx(ctx, im);
    qtask.destroy_req = dr;
    qtask.hypervisor_conns = ctx->hyp_conns;
    write(ctx->virt_pipe, &qtask, sizeof(struct virt_qtask));
    logger_log(LOG_LEVEL_INFO, "msgh", "VM '%s' destroyed by user '%s' @ %s", r->vm_name,
               im->from, ctx->irc_cfg->name);
    vm_ips = db_lookup_vm_ips(ctx->dbc, r->id);
    db_del_vm(ctx->dbc, r->id);
    for(i = 0; i < vm_ips->len; i++) {
        ip_addr = *(uint32_t*)vm_ips->items[i];
        ia.s_addr = htonl(ip_addr);
        virt_dhcp_assign_ip(vc, dom_ptr, inet_ntoa(ia), 1);
    }
    virDomainFree(dom_ptr);
    free(r);
    array_free(vm_ips, 1);
}

void _cb_vm_deploy(struct irc_ctx* ctx, struct irc_privmsg* im, const char* vm_tmpl) {
    array_t* c = NULL;
    char buf[IRC_MSG_LEN], vm_name[VM_NAME_LEN];
    struct vm_creq* creq = NULL;
    struct virt_qtask qtask = {0};
    c = db_lookup_vms_nick(ctx->dbc, ctx->irc_cfg->name, im->from);
    if(c && (CONFIG->vm_user_limit >= 1 && c->len >= CONFIG->vm_user_limit)) {
        snprintf(buf, sizeof(buf),
                 "%s: VM limit reached, delete a VM to deploy a new one or contact an admin to discuss additional options", im->from);
        irc_send_msg(ctx->ns, im->channel, buf);
    } else if(table_lookup_key(ctx->pending_deployments, im->from)) {
        snprintf(buf, sizeof(buf),
                 "%s: another VM is being deployed, please wait!", im->from);
        irc_send_msg(ctx->ns, im->channel, buf);
    } else if(vm_new_name(vm_name)) {
        creq = malloc(sizeof(struct vm_creq));
        creq->vm_name = strdup(vm_name);
        creq->vm_tmpl = ctx->vm_template;
        creq->cdisk_img = strdup(vm_tmpl);
        creq->disk_mb = CONFIG->vm_disk_mb;
        qtask.type = VTQ_CREATE;
        qtask.creq = creq;
        qtask.task_comp_cb = (void*)_cb_virt_vm_creation;
        qtask.task_err_cb = (void*)qtask_cb_virt_gerr;
        qtask.cb_arg = _new_msgh_cb_ctx(ctx, im);
        qtask.hypervisor_conns = ctx->hyp_conns;
        write(ctx->virt_pipe, &qtask, sizeof(struct virt_qtask));
        table_insert(ctx->pending_deployments, im->from, (void*)1);
    }
    if(c) array_free(c, 1);
}

void _cb_vm_vnc(struct irc_ctx* ctx,
                struct irc_privmsg* im, char* vm_name) {
    struct db_vm_model* r = NULL;
    struct vm_vnc_req* vreq = NULL;
    struct virt_qtask qtask = {0};
    char buf[IRC_MSG_LEN], vnc_pw[8+1];
    msgh_lookup_vm_cmp
    if(!CONFIG->vnc_gw.enabled ||
            !(vreq = malloc(sizeof(struct vm_vnc_req))) ||
            !util_gen_rand_str(vnc_pw, 8, 0)) {
        snprintf(buf, sizeof(buf),
                 "%s: VNC access is disabled or an error has occurred", im->from);
        irc_send_msg(ctx->ns, im->channel, buf);
        return;
    }
    vreq->vm_name = strdup(r->vm_name);
    vreq->hypervisor_id = strdup(r->hypervisor_id);
    vreq->pass = strdup(vnc_pw);
    qtask.type = VTQ_VNC;
    qtask.vnc_req = vreq;
    qtask.task_comp_cb = (void*)qtask_cb_vnc_ok;
    qtask.task_err_cb = (void*)qtask_cb_virt_gerr;
    qtask.cb_arg = _new_msgh_cb_ctx(ctx, im);
    qtask.hypervisor_conns = ctx->hyp_conns;
    qtask.vctx = ctx->vctx;
    write(ctx->virt_pipe, &qtask, sizeof(struct virt_qtask));
    logger_log(LOG_LEVEL_INFO, "msgh", "user, %s @ %s, has requested VNC access for %s/%s", im->from,
               ctx->irc_cfg->name, r->vm_name, r->vm_uuid);
    free(r);
}

void _cb_vm_template(struct irc_ctx* ctx, struct irc_privmsg* im) {
    char* buf = NULL;
    char* tmpls = NULL;
    int l = -1;
    tmpls = array_str_join(CONFIG->templates, ", ");
    l = IRC_MSG_LEN+strlen(tmpls)+1;
    buf = malloc(l);
    snprintf(buf, l, "%s: available templates: [%s]", im->from, tmpls);
    irc_send_msg(ctx->ns, im->channel, buf);
    free(buf);
    free(tmpls);
}

void _cb_default(struct irc_ctx* ctx, struct irc_privmsg* im) {
    char buf[IRC_MSG_LEN];
    snprintf(buf, sizeof(buf),
             "%s: invalid command. Valid commands: [ls, deploy, template, vnc, start, stop, destroy]", im->from);
    irc_send_msg(ctx->ns, im->channel, buf);
}

static void msgh_slogin_err(struct irc_ctx* ctx, struct irc_privmsg* im) {
    char buf[IRC_MSG_LEN];
    snprintf(buf, sizeof(buf),
             "%s: please login via services (%s) first!",
             im->from, ctx->irc_cfg->services_nick);
    irc_send_msg(ctx->ns, im->channel, buf);
}

static void msgh_prune_scbs(table_t* services_cbs) {
    int i = -1;
    struct msgh_cb_sctx* cb = NULL;
    for(i = 0; i < services_cbs->len; i++) {
        cb = table_lookup_index(services_cbs, i);
        if(time(NULL)-cb->insert_time >= 120)
            array_pop(services_cbs, i);
    }
}

void msgh_process_msg(struct irc_ctx* ctx, struct irc_privmsg* im) {
    array_t* msgp = NULL;
    char buf[IRC_MSG_LEN];
    msgp = util_split_str(im->msg, " ");
    if(!strcmp(msgp->items[0]+1, "vm") && msgp->len > 1) {
        if(!strcmp(msgp->items[1], "ls")) {
            _cb_vm_ls(ctx, im);
        } else if(!strcmp(msgp->items[1], "deploy")) {
            if(msgp->len < 3 || !array_has_str(CONFIG->templates, msgp->items[2])) {
                snprintf(buf, sizeof(buf),
                         "%s: missing template argument. Use !vm template for available templates", im->from);
                irc_send_msg(ctx->ns, im->channel, buf);
            } else {
                _cb_vm_deploy(ctx, im, msgp->items[2]);
                irc_send_notice(ctx->ns, im->from, "Deployment of VM initiated, please wait!");
            }
        } else if(!strcmp(msgp->items[1], "start") && msgp->len >= 2) {
            _cb_vm_ss(ctx, im, msgp->items[2], 1);
        } else if(!strcmp(msgp->items[1], "stop") && msgp->len >= 2) {
            _cb_vm_ss(ctx, im, msgp->items[2], 0);
        } else if(!strcmp(msgp->items[1], "vnc") && msgp->len >= 2) {
            _cb_vm_vnc(ctx, im, msgp->items[2]);
        } else if(!strcmp(msgp->items[1], "template")) {
            _cb_vm_template(ctx, im);
        } else if(!strcmp(msgp->items[1], "destroy") && msgp->len >= 2) {
            _cb_vm_destroy(ctx, im, msgp->items[2]);
        }
    } else if(!strcmp(msgp->items[0]+1, "vm")) {
        _cb_default(ctx, im);
    }
    array_free(msgp, 0);
}

void msgh_handle(struct irc_ctx* ctx, struct irc_privmsg* im) {
    struct msgh_cb_sctx* sctx = NULL;
    char *s = NULL;
    if(im->msg[0] != '!') return;
    if(ctx->irc_cfg->services_validation) {
        msgh_prune_scbs(ctx->services_cbs);
        sctx = malloc(sizeof(struct msgh_cb_sctx));
        sctx->ctx = ctx;
        sctx->insert_time = time(NULL);
        sctx->ok_cb = msgh_process_msg, sctx->err_cb = msgh_slogin_err;
        memcpy(&sctx->im, im, sizeof(struct irc_privmsg));
        table_insert(ctx->services_cbs, im->from, sctx);
        s = serv_build_info_cmd(ctx->sctx, im->from);
        net_sock_write(ctx->ns, s, strlen(s));
        free(s);
        return;
    }
    msgh_process_msg(ctx, im);
}
