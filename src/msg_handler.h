/* never will be improved, I'm sure */
#include <libxml/parser.h>
#include "irc.h"
#include "net.h"
#include "db.h"
#ifndef H_MH
#define H_MH
// WHO callback ctx
struct msgh_cb_sctx {
    struct irc_ctx* ctx;
    struct vnc_ctx* vctx;
    struct irc_privmsg im;
    uint32_t insert_time;
    void (*ok_cb)(struct irc_ctx* ctx, struct irc_privmsg* im);
    void (*err_cb)(struct irc_ctx* ctx, struct irc_privmsg* im);
};
struct msgh_cb_ctx {
    char* from_nick;
    char* network;
    char* channel;
//    int fd;
    struct vnc_ctx* vctx;
    struct net_sock* ns;
    sqlite3* dbc;
    table_t* hyp_conns;
    table_t* pending_deployments;
};
void msgh_handle(struct irc_ctx* ctx, struct irc_privmsg* im);
#endif
