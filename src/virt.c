#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <libcutil/table.h>
#include <libvirt/libvirt.h>
#include <libvirt/virterror.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <arpa/inet.h>
#include <poll.h>
#include <unistd.h>
#include <assert.h>
#include "config.h"
#include "logger.h"
#include "virt.h"
#include "vnc.h"
#include "net.h"

static void virt_disconnect_cb(virConnectPtr vc_ptr, int reason, void* o);
static void virt_timer_reconnect_cb(int tid, void* o);
static void virt_daemon_read_cb(int wid, int fd, int ev, struct virt_dctx* dctx);
static struct net_sock* _open_daemon_conn(const char* addr);

struct virt_qtask* virt_new_qtask() {
    struct virt_qtask* q = malloc(sizeof(struct virt_qtask));
    q->type = -1;
    q->creq = NULL;
    q->cb_arg = NULL;
    q->task_comp_cb = NULL, q->task_err_cb = NULL;
    return q;
}
static void virt_timer_reconnect_cb(int tid, void* o) {
    struct virt_dctx* ctx = o;
    virConnectPtr vc = NULL;
    logger_log(LOG_LEVEL_INFO, "virt", "reconnecting to %s", ctx->vc->_id);
    if(!(vc = virConnectOpen(ctx->uri)))
        return;
    virConnectClose(ctx->vc->vc);
    ctx->vc->vc = vc;
    virConnectRegisterCloseCallback(ctx->vc->vc, virt_disconnect_cb, o, NULL);
    virEventRemoveTimeout(tid);
}

static void virt_disconnect_cb(virConnectPtr vc_ptr, int reason, void* o) {
    virEventAddTimeout(5*1000, virt_timer_reconnect_cb, o, NULL);
}

static void virt_timer_daemon_reconnect_cb(int tid, struct virt_dctx* dctx) {
    struct config_hypervisor* hyp_cfg = NULL;
    if(!(hyp_cfg = table_lookup_key(CONFIG->hypervisor, dctx->id)))
        return;
    if(dctx->vc->daemon_conn)
        net_sock_close(dctx->vc->daemon_conn);
    if(!(dctx->vc->daemon_conn = _open_daemon_conn(hyp_cfg->daemon_addr))) {
        logger_log(LOG_LEVEL_ERR, "virt", "%s: failed to connect to daemon", dctx->id);
        return;
    }
#ifdef DEBUG
    if(dctx->vc->daemon_conn->fd != -1)
#endif
        dctx->vc->daemon_wid = virEventAddHandle(dctx->vc->daemon_conn->fd, VIR_EVENT_HANDLE_READABLE,
                               (void*)virt_daemon_read_cb, dctx, NULL);
    logger_log(LOG_LEVEL_INFO, "virt", "%s: connected to daemon", dctx->id);
    virEventRemoveTimeout(tid);
}

static void virt_err_cb(void* o, virErrorPtr err) {
    if(err->code != VIR_ERR_INTERNAL_ERROR && err->code != VIR_ERR_SYSTEM_ERROR)
        logger_log(LOG_LEVEL_ERR, "libvirt", "%d: %s", err->code, err->message);
}

static void virt_daemon_read_cb(int wid, int fd, int ev, struct virt_dctx* dctx) {
    char buf[1];
    if(recv(fd, &buf, 1, 0) > 0)
        return;
    logger_log(LOG_LEVEL_ERR, "virt", "%s: disconnected from daemon", dctx->id);
    virEventAddTimeout(1000, (void*)virt_timer_daemon_reconnect_cb, dctx, NULL);
    virEventRemoveHandle(wid);
}

static struct net_sock* _open_daemon_conn(const char* addr) {
    char* sp = NULL, *hostname = NULL, *tmp = NULL, *a = NULL;
    uint16_t port = 0;
    struct net_sock* ns = NULL;
    a = malloc(strlen(addr)+1);
    strcpy(a, addr);
    if(!(hostname = strtok_r((char*)a, ":", &tmp)) || !(tmp = strtok_r(NULL, ":", &tmp)) || (port = atoi(tmp)) < 1) {
        free(a);
        return NULL;
    }
    ns = net_sock_connect(hostname, port, 0);
    free(sp);
    free(a);
    return ns;
}

struct virt_conns_ctx* virt_open_hyp_conns(table_t* hypervisors) {
    struct virt_conns_ctx* ctx = NULL;
    struct config_hypervisor* ch = NULL;
    struct virt_conn* vc = NULL;
    struct virt_dctx* dctx = NULL;
    virConnectPtr hyp_conn = NULL;
    virSetErrorFunc(NULL, virt_err_cb);
    if(virEventRegisterDefaultImpl() < 0)
        return NULL;
    ctx = malloc(sizeof(struct virt_conns_ctx));
    ctx->hyp_conns = table_new();
    for(int i = 0; i < hypervisors->len; i++) {
        ch = table_lookup_index(hypervisors, i);
        if(!(hyp_conn = virConnectOpen(ch->uri)))
            continue;
        if(ch->keepalive && virConnectSetKeepAlive(hyp_conn, 5, 6) < 0)
            continue;
        vc = malloc(sizeof(struct virt_conn));
        vc->vc = hyp_conn;
        if(!(vc->daemon_conn = _open_daemon_conn(ch->daemon_addr))) {
#ifndef DEBUG
            break;
#else
            logger_log(LOG_LEVEL_DEBUG, "virt", "%s: failed to connect to daemon, ignoring", ch->id);
            vc->daemon_conn = malloc(sizeof(struct net_sock));
            vc->daemon_conn->fd = -1;
#endif
        }
        vc->_id = ch->id;
        dctx = malloc(sizeof(struct virt_dctx));
        strcpy(dctx->uri, ch->uri);
        strcpy(dctx->id, ch->id);
        dctx->vc = vc;
        dctx->vctx = ctx;
        vc->dctx = dctx;
        virConnectRegisterCloseCallback(hyp_conn, virt_disconnect_cb, dctx, NULL);
#ifdef DEBUG
        if(vc->daemon_conn->fd != -1)
#endif
            vc->daemon_wid = virEventAddHandle(vc->daemon_conn->fd, VIR_EVENT_HANDLE_READABLE,
                                               (void*)virt_daemon_read_cb, dctx, NULL);
        table_insert(ctx->hyp_conns, ch->id, vc);
    }
    if(ctx->hyp_conns->len < 1) {
        free(ctx->hyp_conns);
        return NULL;
    }
    return ctx;
}

xmlDocPtr virt_open_template(char* filename) {
    return xmlParseFile(filename);
}

struct virt_vnc_info* virt_get_vm_vnc_port(table_t* hyp_conns, struct vm_vnc_req* vreq) {
    struct virt_conn* vc = NULL;
    struct virt_vnc_info* vi = NULL;
    struct config_hypervisor* chyp = NULL;
    char* dxml = NULL, *sport = NULL;
    int c = -1;
    virDomainPtr dom_ptr = NULL;
    xmlXPathContextPtr xpath_ctx;
    xmlDocPtr xdoc = NULL;
    xmlXPathObjectPtr xpath_obj = NULL;
    if(!(vc = table_lookup_key(hyp_conns, vreq->hypervisor_id))
            || !(chyp = table_lookup_key(CONFIG->hypervisor, vreq->hypervisor_id))) {
        c = 0;
        goto ret;
    }
    if(!(dom_ptr = virDomainLookupByName(vc->vc, vreq->vm_name))) {
        c = 0;
        goto ret;
    }
    if(!(dxml = virDomainGetXMLDesc(dom_ptr, 0))) {
        c = 0;
        goto ret;
    }
    if(!(xdoc = xmlParseMemory(dxml, strlen(dxml)))) {
        c = 0;
        goto ret;
    }
    if(!(xpath_ctx = xmlXPathNewContext(xdoc))) {
        c = 0;
        goto ret;
    }
    if(!(xpath_obj = xmlXPathEvalExpression((xmlChar*)"//domain/devices/graphics[@type='vnc']", xpath_ctx))
            || !(sport = (char*)xmlGetProp(xpath_obj->nodesetval->nodeTab[0], (xmlChar*)"port")))  {
        c = 0;
        goto ret;
    }
    c = atoi(sport);
    vi = malloc(sizeof(struct virt_vnc_info));
    vi->port = c;
    vi->pass = vreq->pass;
    strncpy(vi->hyp_addr, chyp->vnc_ln_addr, sizeof(vi->hyp_addr));
ret:
    if(xpath_obj)
        xmlXPathFreeObject(xpath_obj);
    if(xpath_ctx)
        xmlXPathFreeContext(xpath_ctx);
    if(xdoc)
        xmlFreeDoc(xdoc);
    if(sport)
        xmlFree(sport);
    if(dxml)
        free(dxml);
    if(dom_ptr)
        virDomainFree(dom_ptr);
    return vi;
}

short virt_dhcp_assign_ip(struct virt_conn* vc, virDomainPtr vm, const char* ip_addr, short unset) {
    assert(vm && vc && vc->daemon_conn);
    char buf[1024] = {0}, *dxml = NULL, *mac_addr = NULL;
    xmlXPathContextPtr xpath_ctx;
    xmlDocPtr xdoc = NULL;
    xmlXPathObjectPtr xpath_obj = NULL;
    int i = 0;
    if(!(dxml = virDomainGetXMLDesc(vm, 0)))
        return 0;
    if(!(xdoc = xmlParseMemory(dxml, strlen(dxml)))) {
        free(dxml);
        return 0;
    }
    free(dxml);
    if(!(xpath_ctx = xmlXPathNewContext(xdoc)))
        return 0;
    if(!(xpath_obj = xmlXPathEvalExpression((xmlChar*)"//domain/devices/interface[@type='network']/mac", xpath_ctx)) ||
            !(mac_addr = (char*)xmlGetProp(xpath_obj->nodesetval->nodeTab[0], (xmlChar*)"address"))) {
        xmlXPathFreeContext(xpath_ctx);
        xmlFreeDoc(xdoc);
        return 0;
    }
    buf[0] = !unset ? 0 : 1;
    i += strlen(mac_addr);
    memcpy(buf+1, mac_addr, i);
    buf[i+1] = strlen(ip_addr);
    memcpy(buf+i+2, ip_addr, strlen(ip_addr));
    xmlXPathFreeObject(xpath_obj);
    xmlFree(mac_addr);
    xmlXPathFreeContext(xpath_ctx);
    xmlFreeDoc(xdoc);
    return net_sock_write(vc->daemon_conn, buf, 1024) == 1024 ? 1 : 0;
}

// Password argument must be in plain-text
short virt_assign_pass_port(struct virt_conn* vc,
                            const char* vm_ip, const char* password, uint16_t admin_port) {
    char buf[1024] = {0};
    unsigned int plen = 0, iplen = 0;
    admin_port = htons(admin_port);
    // IPv6, maaaybe?
    if((plen = strlen(password)) >= 60 || (iplen = strlen(vm_ip)) >= 36)
        return 0;
    buf[0] = 2;
    buf[1] = iplen & 0xFF;
    memcpy(buf+2, vm_ip, iplen);
    buf[iplen+2] = plen & 0xFF;
    memcpy(buf+(iplen+3), password, plen);
    buf[iplen+plen+3] = admin_port & 0xFF;
    buf[iplen+plen+4] = (admin_port >> 8) & 0xFF;
    return net_sock_write(vc->daemon_conn, buf, 1024) == 1024 ? 1 : 0;
}

short virt_destroy_vm(table_t* hyp_conns, const struct vm_destroy_req* destroy_req) {
    struct virt_conn* vc = NULL;
    struct config_hypervisor* hyp = NULL;
    virDomainPtr dom_ptr = NULL;
    virStoragePoolPtr pool_ptr = NULL;
    virStorageVolPtr vol_ptr = NULL;
    short ok = -1;
    if(!(vc = table_lookup_key(hyp_conns, destroy_req->hypervisor_id)))
        return 0;
    if(!(hyp = table_lookup_key(CONFIG->hypervisor, destroy_req->hypervisor_id)) ||
            !(pool_ptr = virStoragePoolLookupByName(vc->vc, hyp->diskstore)))
        return 0;
    if(!(dom_ptr = virDomainLookupByName(vc->vc, destroy_req->vm_name)))
        return 0;
    virDomainDestroy(dom_ptr);
    ok = virDomainUndefine(dom_ptr);
    if(ok < 0)
        return 0;
    virDomainFree(dom_ptr);
    if(!(vol_ptr = virStorageVolLookupByName(pool_ptr, destroy_req->vm_name)))
        return 0;
    ok = virStorageVolDelete(vol_ptr, 0);
    virStorageVolFree(vol_ptr);
    virStoragePoolFree(pool_ptr);
    return ok == -1 ? 0 : 1;
}

/* Return codes:
 * 0 => error
 * 1 => ok
 * 2 => failed to start because it is already running
 */
short virt_ss_vm(table_t* hyp_conns, const char* hypervisor, const char* vm_uuid, const short start) {
    struct virt_conn* vc = NULL;
    virDomainPtr dom_ptr = NULL;
    virDomainInfoPtr di = NULL;
    int i = -1;
    if(!(vc = table_lookup_key(hyp_conns, hypervisor)))
        return 0;
    if(!(dom_ptr = virDomainLookupByUUIDString(vc->vc, vm_uuid)))
        return 0;
    di = malloc(sizeof(virDomainInfo));
    if(virDomainGetInfo(dom_ptr, di) < 0 ||
            ((start && di->state != VIR_DOMAIN_SHUTOFF) ||
             (!start && di->state != VIR_DOMAIN_RUNNING))) {
        free(di);
        return 2;
    }
    free(di);
    i = ((start && virDomainCreate(dom_ptr) < 0) || (!start && virDomainDestroy(dom_ptr) < 0));
    virDomainFree(dom_ptr);
    return !i ? 1 : 0;
}

// Returns NULL on error, resize_mb can be set to 0 for the resize operation to not take affect
char* virt_clone_vol(virConnectPtr hyp_conn, struct config_hypervisor_tmpl* cvol_tmpl,
                     const char* vol_name, const char* dst_diskstore, uint32_t resize_mb) {
    virStorageVolPtr cvol = NULL, vol = NULL;
    virStoragePoolPtr cstore_pool = NULL, store_pool = NULL;
    virStorageVolInfo vi;
    xmlBufferPtr xbuf = xmlBufferCreate();
    xmlNodePtr volume = NULL, vtarget = NULL, vformat = NULL;
    char* sv_path = NULL;
    if(!(store_pool = virStoragePoolLookupByName(hyp_conn, dst_diskstore)) ||
            !(cstore_pool = virStoragePoolLookupByName(hyp_conn, cvol_tmpl->store_pool))) {
        return NULL;
    }
    volume = xmlNewNode(NULL, (xmlChar*)"volume");
    vtarget = xmlNewNode(NULL, (xmlChar*)"target");
    vformat = xmlNewNode(NULL, (xmlChar*)"format");
    xmlSetProp(vformat, (xmlChar*)"type", (xmlChar*)CONFIG->template_format);
    xmlNewTextChild(volume, NULL, (xmlChar*)"name", (xmlChar*)vol_name);
    xmlAddChild(vtarget, vformat);
    xmlAddChild(volume, vtarget);
    if(xmlNodeDump(xbuf, NULL, volume, 0, 1) > 0 && (cvol = virStorageVolLookupByName(cstore_pool, cvol_tmpl->img_filename))) {
        if(virStorageVolGetInfo(cvol, &vi) < 0) {
            return NULL;
        }
        vol = virStorageVolCreateXMLFrom(store_pool, (char*)xmlBufferContent(xbuf), cvol, 0);
        // resize the volume, if required...
        sv_path = virStorageVolGetPath(vol);
        if(resize_mb > 0)
            virStorageVolResize(vol, ((long)resize_mb)*1024*1024, 0);
    }
    if(cvol != NULL)
        virStorageVolFree(cvol);
    if(vol != NULL)
        virStorageVolFree(vol);
    virStoragePoolFree(store_pool);
    virStoragePoolFree(cstore_pool);
    xmlFreeNode(volume);
    xmlBufferFree(xbuf);
    return sv_path;
}

// Returns NULL on error
char* virt_create_svol(virConnectPtr vc, struct config_hypervisor* hyp, uint32_t size_mb, char* sname) {
    xmlNodePtr volume, vcapacity;
    xmlBufferPtr xbuf = NULL;
    virStoragePoolPtr store_pool = NULL;
    virStorageVolPtr sv = NULL;
    char sint[sizeof(uint32_t)+1], *s = NULL;
    if(!(store_pool = virStoragePoolLookupByName(vc, hyp->diskstore)))
        return NULL;
    xbuf = xmlBufferCreate();
    volume = xmlNewNode(NULL, (xmlChar*)"volume");
    xmlNewTextChild(volume, NULL, (xmlChar*)"name", (xmlChar*)sname);
    xmlNewTextChild(volume, NULL, (xmlChar*)"allocation", (xmlChar*)"0");
    sprintf(sint, "%d", size_mb);
    vcapacity = xmlNewTextChild(volume, NULL, (xmlChar*)"capacity", (xmlChar*)sint);
    xmlSetProp(vcapacity, (xmlChar*)"unit", (xmlChar*)"M");
    if(xmlNodeDump(xbuf, NULL, volume, 0, 1) > 0) {
        if((sv = virStorageVolCreateXML(store_pool, (char*)xmlBufferContent(xbuf), 0))) {
            s = virStorageVolGetPath(sv);
            virStorageVolFree(sv);
        }
    }

    virStoragePoolFree(store_pool);
    xmlFreeNode(volume);
    xmlBufferFree(xbuf);
    return s;
}

const struct vm_cresp* virt_create_vm(table_t* hypervisor_conns, struct vm_creq* creq) {
    int i = -1;
    virConnectPtr vc = NULL;
    virDomainPtr vm_ins = NULL;
    xmlXPathContextPtr xpath_ctx;
    xmlXPathObjectPtr xpath_obj;
    struct config_hypervisor* hyp;
    struct virt_conn* vc_tmp = NULL;
    struct config_hypervisor_tmpl* cdisk_img = NULL;
    char* sv_path = NULL, *xstr;
    struct vm_cresp* s = NULL;
    assert(creq->vm_name && creq->vm_tmpl && hypervisor_conns);
    for(i = 0; i < CONFIG->hypervisor->len; i++) {
        hyp = table_lookup_index(CONFIG->hypervisor, i);
        if(!(vc_tmp = table_lookup_key(hypervisor_conns, hyp->id)) || !vc_tmp->vc)
            continue;
        // TODO: erh.. replace this with something better when the time comes
        // For instance, taking the memory required by the template and checking to see if we have that + 512MB of memory free, would be a better idea
        vc = vc_tmp->vc;
        if(virNodeGetFreeMemory(vc)/1024 < 1024)
            continue;
        if(creq->cdisk_img && !(cdisk_img = table_lookup_key(hyp->templates, creq->cdisk_img)))
            continue;
        break;
    }
    if(!vc) {
        logger_log(LOG_LEVEL_ERR, "virt", "virt_create_vm(): failed to find a free hypervisor node");
        return 0;
    }
    if(!creq->cdisk_img && !(sv_path = virt_create_svol(vc, hyp, creq->disk_mb, creq->vm_name))) {
        logger_log(LOG_LEVEL_ERR, "virt", "failed to allocate VM disk image on hypervisor '%s': %s", hyp->id, creq->vm_name);
        return 0;
    } else if(creq->cdisk_img) {
        sv_path = virt_clone_vol(vc, cdisk_img, creq->vm_name, hyp->diskstore, creq->disk_mb);
    }
    if(!(xpath_ctx = xmlXPathNewContext(creq->vm_tmpl)))
        return 0;
    // >:(
    if(!(xpath_obj = xmlXPathEvalExpression((xmlChar*)"//domain/name", xpath_ctx)) || xpath_obj->nodesetval->nodeNr < 1)
        goto err;
    xmlNodeSetContent(xpath_obj->nodesetval->nodeTab[0], (xmlChar*)creq->vm_name);
    xmlXPathFreeObject(xpath_obj);
    if(!(xpath_obj = xmlXPathEvalExpression((xmlChar*)"//domain/devices/interface[@type='network']/source", xpath_ctx)) || xpath_obj->nodesetval->nodeNr < 1)
        goto err;
    xmlSetProp(xpath_obj->nodesetval->nodeTab[0], (xmlChar*)"network", (xmlChar*)hyp->network);
    xmlXPathFreeObject(xpath_obj);
    if(creq->cdisk_img) {
        if(!(xpath_obj = xmlXPathEvalExpression((xmlChar*)"//devices/disk[@device='disk']/driver", xpath_ctx)) || xpath_obj->nodesetval->nodeNr < 1)
            goto err;
        xmlSetProp(xpath_obj->nodesetval->nodeTab[0], (xmlChar*)"type", (xmlChar*)CONFIG->template_format);
        xmlXPathFreeObject(xpath_obj);
    }
    if(!(xpath_obj = xmlXPathEvalExpression((xmlChar*)"//devices/disk[@device='disk']/source", xpath_ctx)) || xpath_obj->nodesetval->nodeNr < 1)
        goto err;
    xmlSetProp(xpath_obj->nodesetval->nodeTab[0], (xmlChar*)"file", (xmlChar*)sv_path);
    xmlXPathFreeObject(xpath_obj);

    xmlXPathFreeContext(xpath_ctx);
    xmlDocDumpMemory(creq->vm_tmpl, (xmlChar**)&xstr, NULL);
    // ok, ignore the above mess, now we create the VM instance...
    if((vm_ins = virDomainDefineXML(vc, xstr))) {
        s = malloc(sizeof(struct vm_cresp));
        s->uuid = malloc(VIR_UUID_STRING_BUFLEN+1);
        virDomainGetUUIDString(vm_ins, s->uuid);
        s->vm_name = strdup(creq->vm_name);
        s->hypervisor = hyp->id, s->dom_ptr = vm_ins;
    }
    xmlFree(xstr);
    free(sv_path);
    return s;
err:
    if(xpath_ctx != NULL)
        free(xpath_ctx);
    if(xpath_obj != NULL)
        free(xpath_obj);
    return NULL;
}

void virt_thread_worker(int* virt_pipe) {
    int i = -1;
    struct virt_qtask* qtask = malloc(sizeof(struct virt_qtask));
    struct virt_vnc_info* vi = NULL;
    void* c = NULL;
    while(read(*virt_pipe, qtask, sizeof(struct virt_qtask)) > 0) {
        c = NULL;
        switch(qtask->type) {
        case VTQ_CREATE:
            if((c = (void*)virt_create_vm(qtask->hypervisor_conns, qtask->creq)))
                qtask->task_comp_cb(c, qtask->cb_arg);
            else
                qtask->task_err_cb(c, qtask->cb_arg);
            free(qtask->creq->cdisk_img);
            free(qtask->creq->vm_name);
            free(qtask->creq);
            break;
        case VTQ_SS:
            if((i = virt_ss_vm(qtask->hypervisor_conns,
                               qtask->start_req->hypervisor_id, qtask->start_req->vm_uuid,
                               qtask->start_req->start)))
                qtask->task_comp_cb(qtask->start_req, qtask->cb_arg);
            else
                qtask->task_err_cb(qtask->start_req, qtask->cb_arg);
            free(qtask->start_req->hypervisor_id);
            free(qtask->start_req->vm_uuid);
            free(qtask->start_req);
            break;
        case VTQ_VNC:
            vi = virt_get_vm_vnc_port(qtask->hypervisor_conns, qtask->vnc_req);
            if(!vi)
                qtask->task_err_cb(qtask->vnc_req, qtask->cb_arg);
            else if(vnc_send_host_msg(qtask->vctx, vi->hyp_addr, vi->pass, vi->port)) {
                qtask->task_comp_cb(vi, qtask->cb_arg);
                free(vi);
            }
            free(qtask->vnc_req->pass);
            free(qtask->vnc_req->vm_name);
            free(qtask->vnc_req->hypervisor_id);
            free(qtask->vnc_req);
            break;
        case VTQ_DESTROY:
            if(virt_destroy_vm(qtask->hypervisor_conns, qtask->destroy_req))
                qtask->task_comp_cb(qtask->destroy_req, qtask->cb_arg);
            else
                qtask->task_err_cb(qtask->destroy_req, qtask->cb_arg);
            free(qtask->destroy_req->hypervisor_id);
            free(qtask->destroy_req->vm_name);
            free(qtask->destroy_req);
            break;
        }
    }
    free(qtask);
}

void virt_cleanup(struct virt_conns_ctx* hyp_ctx) {
    struct virt_conn* h = NULL;
    int i = -1;
    for(i = 0; i < hyp_ctx->hyp_conns->len; i++) {
        h = table_lookup_index(hyp_ctx->hyp_conns, i);
        virConnSetErrorFunc(h->vc, NULL, NULL);
        virConnectUnregisterCloseCallback(h->vc, virt_disconnect_cb);
        free(h->dctx);
        virConnectClose(h->vc);
#ifdef DEBUG
        if(h->daemon_conn->fd != -1) {
#endif
            virEventRemoveHandle(h->daemon_wid);
            net_sock_close(h->daemon_conn);
#ifdef DEBUG
        }
#endif
    }
    table_free(hyp_ctx->hyp_conns, 1);
    free(hyp_ctx);
}
