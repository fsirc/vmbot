#include <sqlite3.h>
#include <libcutil/array.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <arpa/inet.h>
#include "ip.h"
#include "db.h"
#include "logger.h"
#include "config.h"

sqlite3* db_open() {
    sqlite3* db = NULL;
    FILE* sql_file = NULL;
    char *s = NULL, *err_msg = NULL;
    int r = -1;
    if(sqlite3_open("db.db", &db)) {
        // err, uh..
        return NULL;
    }
    if((sql_file = fopen("db.sql", "r")) == NULL)
        return NULL;
    fseek(sql_file, 0, SEEK_END);
    r = ftell(sql_file);
    if(!(s = malloc(r+1))) {
        fclose(sql_file);
        logger_log(LOG_LEVEL_ERR, "db", "failed to read db.sql file");
        return NULL;
    }
    rewind(sql_file);
    r = fread(s, 1, r, sql_file);
    s[r++] = '\0';
    fclose(sql_file);
    r = sqlite3_exec(db, s, NULL, NULL, &err_msg);
    free(s);
    if(r) {
        logger_log(LOG_LEVEL_ERR, "db", "failed to create tables: %s", err_msg);
        free(err_msg);
        return NULL;
    }
    return db;
}

struct db_vm_model* db_lookup_vm(sqlite3* dbc, const char* vm_name) {
    sqlite3_stmt* stmt;
    struct db_vm_model* r = NULL;
    if(sqlite3_prepare_v2(dbc, "SELECT id, name, hypervisor_id, vm_uuid, owner, network, creation_date FROM vm WHERE name = ?", -1, &stmt, NULL))
        return 0;
    sqlite3_bind_text(stmt, 1, vm_name, -1, NULL);
    if(sqlite3_step(stmt) == SQLITE_ROW) {
        r = malloc(sizeof(struct db_vm_model));
        r->id = sqlite3_column_int(stmt, 0);
        strncpy(r->vm_name, (char*)sqlite3_column_text(stmt, 1), 20);
        strncpy(r->hypervisor_id, (char*)sqlite3_column_text(stmt, 2), 20);
        strncpy(r->vm_uuid, (char*)sqlite3_column_text(stmt, 3), 37);
        strncpy(r->owner, (char*)sqlite3_column_text(stmt, 4), 31);
        strncpy(r->network, (char*)sqlite3_column_text(stmt, 5), 60);
        r->creation_date = sqlite3_column_int64(stmt, 6);
    }
    sqlite3_finalize(stmt);
    return r;
}

short db_nick_owns_vm_by_name(sqlite3* dbc, const char* nick, const char* vm_name) {
    struct db_vm_model* r = NULL;
    if(!(r = db_lookup_vm(dbc, vm_name)) || strcmp(r->owner, nick) != 0) {
        if(r) free(r);
        return 0;
    }
    return 1;
}

array_t* db_lookup_vms_nick(sqlite3* dbc, const char* network, const char* nick) {
    sqlite3_stmt* stmt;
    array_t* rows;
    struct db_vm_model* r = NULL;
    if(sqlite3_prepare_v2(dbc, "SELECT id, name, hypervisor_id, vm_uuid, creation_date FROM vm WHERE owner = ? AND network = ?", -1, &stmt, NULL)) {
        return NULL;
    }
    sqlite3_bind_text(stmt, 1, nick, -1, NULL);
    sqlite3_bind_text(stmt, 2, network, -1, NULL);
    rows = array_new();
    while(sqlite3_step(stmt) == SQLITE_ROW) {
        r = malloc(sizeof(struct db_vm_model));
        r->id = sqlite3_column_int(stmt, 0);
        strncpy(r->vm_name, (char*)sqlite3_column_text(stmt, 1), 20);
        strncpy(r->hypervisor_id, (char*)sqlite3_column_text(stmt, 2), 20);
        strncpy(r->vm_uuid, (char*)sqlite3_column_text(stmt, 3), 37);
        r->creation_date = sqlite3_column_int64(stmt, 4);
        array_insert(rows, r);
    }
    sqlite3_finalize(stmt);
    if(rows->len < 1) {
        array_free(rows, 0);
        return NULL;
    }
    return rows;
}
// Returns VM ID on success, returns -1 on error
int db_insert_vm(sqlite3* dbc, const struct db_vm_model* vm_model) {
    sqlite3_stmt* stmt;
    if(sqlite3_prepare_v2(dbc, "INSERT INTO vm (name, network, hypervisor_id, vm_uuid, owner, creation_date) VALUES (?, ?, ?, ?, ?, ?)", -1, &stmt, NULL))
        return -1;
    sqlite3_bind_text(stmt, 1, vm_model->vm_name, -1, NULL);
    sqlite3_bind_text(stmt, 2, vm_model->network, -1, NULL);
    sqlite3_bind_text(stmt, 3, vm_model->hypervisor_id, -1, NULL);
    sqlite3_bind_text(stmt, 4, vm_model->vm_uuid, -1, NULL);
    sqlite3_bind_text(stmt, 5, vm_model->owner, -1, NULL);
    sqlite3_bind_int64(stmt, 6, time(NULL));
    if(sqlite3_step(stmt) != SQLITE_DONE)
        return -1;
    sqlite3_finalize(stmt);
    return (int)sqlite3_last_insert_rowid(dbc);
}

short db_del_vm(sqlite3* dbc, uint32_t vm_id) {
    char buf[100+1];
    snprintf(buf, sizeof(buf),
             "DELETE FROM vm WHERE id = %u; DELETE FROM vm_ip WHERE vm_id = %u", vm_id, vm_id);
    if(sqlite3_exec(dbc, buf, NULL, NULL, NULL)) {
        return 0;
    }
    return 1;
}

short db_del_vm_ip(sqlite3* dbc, uint32_t vm_id, uint32_t vm_ip) {
    char buf[100+1];
    snprintf(buf, sizeof(buf),
             "DELETE FROM vm_ip WHERE vm_ip = %u AND vm_id = %u", vm_ip, vm_id);
    if(sqlite3_exec(dbc, buf, NULL, NULL, NULL)) {
        return 0;
    }
    return 1;
}

short db_insert_vm_ip(sqlite3* dbc, const struct db_vm_ip_model* ip_model) {
    sqlite3_stmt* stmt;
    if(sqlite3_prepare_v2(dbc, "INSERT INTO vm_ip(vm_id, ip_addr) VALUES (?, ?)", -1, &stmt, NULL))
        return 0;
    sqlite3_bind_int(stmt, 1, ip_model->vm_id);
    sqlite3_bind_int64(stmt, 2, ip_model->ip_addr);
    if(sqlite3_step(stmt) != SQLITE_DONE)
        return 0;
    sqlite3_finalize(stmt);
    return 1;
}

array_t* db_lookup_vm_ips(sqlite3* dbc, uint32_t vm_id) {
    sqlite3_stmt* stmt = NULL;
    array_t* vm_ips = array_new();
    int i = -1;
    uint64_t* ip = NULL;
    if(sqlite3_prepare_v2(dbc, "SELECT ip_addr FROM vm_ip WHERE vm_id = ?", -1, &stmt, NULL))
        return vm_ips;
    sqlite3_bind_int64(stmt, 1, vm_id);
    while(sqlite3_step(stmt) == SQLITE_ROW) {
        i = sqlite3_column_int64(stmt, 0);
        ip = malloc(sizeof(uint64_t));
        memcpy(ip, &i, sizeof(i));
        array_insert(vm_ips, ip);
    }
    sqlite3_finalize(stmt);
    return vm_ips;
}

struct db_vm_ip_alloc* db_get_free_ip(sqlite3* dbc,
                                      const struct ip_range* irange, const char* hypervisor) {
    sqlite3_stmt* stmt = NULL;
    struct in_addr ia;
    struct db_vm_ip_alloc* ip_alloc = NULL;
    char* nat_pub_ip = NULL;
    uint32_t last_addr = 0, b = 2;
    if(sqlite3_prepare_v2(dbc, "SELECT ip_addr FROM vm_ip LEFT JOIN vm ON vm.id = vm_ip.vm_id WHERE vm.hypervisor_id = ? AND vm_ip.ip_addr > ? AND vm_ip.ip_addr <= ? ORDER BY vm_ip.ip_addr ASC", -1, &stmt, NULL))
        return NULL;
    sqlite3_bind_text(stmt, 1, hypervisor, -1, NULL);
    sqlite3_bind_int64(stmt, 2, irange->network_addr);
    sqlite3_bind_int64(stmt, 3, irange->broadcast_addr);
    for(int i = b; i < irange->host_count; i++) {
        last_addr = irange->network_addr|i;
        if(sqlite3_step(stmt) == SQLITE_ROW && sqlite3_column_int64(stmt, 0) == last_addr) {
            continue;
        }
        ia.s_addr = ntohl(last_addr);
        ip_alloc = malloc(sizeof(struct db_vm_ip_alloc));
        ip_alloc->nat_pub_ip = NULL, ip_alloc->port_admin = 2222;
        ip_alloc->port_r_end = 65535;
        ip_alloc->port_r_start = 0;
        if(CONFIG->nat.enabled && table_lookup_key(CONFIG->nat.ranges, hypervisor) &&
                (nat_pub_ip = table_lookup_key(table_lookup_key(CONFIG->nat.ranges, hypervisor),
                                               &irange->network_addr))) {
            ip_alloc->port_r_end = ip_get_port_range(CONFIG->nat.start_port, i-1);
            ip_alloc->port_r_start = (ip_alloc->port_r_end-IP_PORT_RANGE_CNT)+1;
            ip_alloc->port_admin = ip_alloc->port_r_start;
            ip_alloc->nat_pub_ip = nat_pub_ip;
        }
        ip_alloc->ip_addr = inet_ntoa(ia);
        break;
    }
    sqlite3_finalize(stmt);
    return ip_alloc;
}
