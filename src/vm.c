#include <stdio.h>
#include <assert.h>
#include "util.h"
#include "vm.h"

short vm_new_name(char* buf) {
    char rdata[VM_NAME_LEN];
    if(!util_gen_rand_str(rdata, VM_RNAME_LEN+1, 1))
        return 0;
    if(!snprintf(buf, VM_NAME_LEN, "%s%s", VM_NAME_PREFIX, rdata))
        return 0;
    return 1;
}
