#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <libvirt/libvirt.h>
#include <libcutil/util.h>
#include "net.h"
#include "logger.h"
#include "config.h"
#include "vnc.h"

#define VNC_ADD_HOST_FMT "add_host %s %u %s 1 0\r\n"
#define VNC_GW_BANNER "VNCGateway"
static void vnc_delay_connect_cb(int timer_id, struct vnc_ctx* ctx);

struct vnc_ctx* vnc_new_ctx() {
    struct vnc_ctx* ctx = malloc(sizeof(struct vnc_ctx));
    memset(ctx, 0, sizeof(struct vnc_ctx));
    return ctx;
}

void vnc_free_ctx(struct vnc_ctx* ctx) {
    net_sock_close(ctx->ns);
    free(ctx);
}

static void vnc_delay_connect_cb(int timer_id, struct vnc_ctx* ctx) {
    vnc_connect(ctx);
    virEventRemoveTimeout(timer_id);
}

void vnc_connect(struct vnc_ctx* ctx) {
    struct net_sock* ns = NULL;
    if(!CONFIG->vnc_gw.enabled)
        return;
    ctx->is_authed = 0;
    ns = net_sock_connect(CONFIG->vnc_gw.addr,
                          CONFIG->vnc_gw.port, 0);
    if(!ns) {
        logger_log(LOG_LEVEL_INFO, "vnc", "failed to connect to gateway");
        virEventAddTimeout(2000, (void*)vnc_delay_connect_cb, ctx, NULL);
        return;
    }
    ctx->ns = ns;
    virEventAddHandle(ctx->ns->fd, VIR_EVENT_HANDLE_READABLE,
                      (void*)vnc_read_cb, ctx, NULL);

}

short vnc_send_host_msg(struct vnc_ctx* ctx, char* host, char* pass, uint16_t port) {
    if(!dprintf(ctx->ns->fd, VNC_ADD_HOST_FMT, host, port, pass)) {
        return 0;
    }
    return 1;
}

void vnc_read_cb(int wid, int fd, int ev, struct vnc_ctx* ctx) {
    // bit genereous, heh?
    char buf[320+1];
    if(!cutil_read_line(fd, buf, sizeof(buf))) {
        logger_log(LOG_LEVEL_INFO, "vnc", "disconnected from gateway");
        vnc_connect(ctx);
        virEventRemoveHandle(wid);
        return;
    }
    if(!ctx->is_authed) {
        if(strstr(buf, VNC_GW_BANNER)) {
            dprintf(ctx->ns->fd, "pass %s\r\n", CONFIG->vnc_gw.pass);
        } else if(!strncmp(buf, "pass", 4)) {
            ctx->is_authed = 1;
            logger_log(LOG_LEVEL_INFO, "vnc",
                       "authenticated with gateway");
        } else if(!strncmp(buf, "ERROR", 5)) {
            logger_log(LOG_LEVEL_INFO, "vnc",
                       "incorrect gateway password provided, disabling gateway");
            CONFIG->vnc_gw.enabled = 0;
            close(fd);
            virEventRemoveHandle(wid);
            return;
        }
    }
}
