#include <libcutil/array.h>
#ifndef H_UTIL
#define H_UTIL
#define util_arr_size(x) sizeof(x)/sizeof(x[0])
array_t* util_split_str(char* str, char* delim);
short util_gen_rand_str(char* buf, uint16_t len, unsigned short lowercase);
#endif
