#include <gnutls/gnutls.h>
#include <libxml/parser.h>
#include <sqlite3.h>
#include "net.h"
#include "services.h"
#include "config.h"

#ifndef H_IRC
#define H_IRC
#define IRC_MSG_LEN 512+1
#define IRC_CHAN_LEN 50+1
#define IRC_NICK_LEN 31+1
struct msgh_ctx;
enum irc_msg_type {
    IMSG_MOTD_END,
    IMSG_SERR,
    IMSG_PM,
    IMSG_MODE,
    IMSG_CHANMSG,
    IMSG_NOTICE,
    IMSG_PING,
    IMSG_NICK,
    // services
    IMSG_WHO,
    // if the connection is interrupted...
    IMSG_EOF,
};
// Yes, upon initialisation of the bot, we create two callback watchers (libvirt) and upon disconnection, we destroy the watcher and initialise a new one. Upon exit, destroy them all!
struct irc_wevs {
    int read_id; // read watcher
    int conn_id;
    int reg_id; // registration timer id, call every 5s
};
struct irc_ctx {
    uint32_t start_time;
    short registered;
    short services_pstart;
    int virt_pipe;
    struct irc_wevs wevs;
    struct serv_ctx* sctx;
    struct config_irc* irc_cfg;
    sqlite3* dbc;
    struct net_sock* ns;
    struct vnc_ctx* vctx;
    table_t* hyp_conns;
    table_t* pending_deployments;
    // services verification
    table_t* services_cbs;
    xmlDocPtr vm_template;
};
struct irc_msg {
    enum irc_msg_type msg_type;
};
struct irc_msg_mode {
    enum irc_msg_type msg_type;
    char target[50+1];
    char modes[50+1];
};
struct irc_msg_ping {
    enum irc_msg_type msg_type;
    char msg[20];
};
struct irc_msg_nick {
    enum irc_msg_type msg_type;
    char nick[IRC_NICK_LEN];
};
struct irc_msg_who {
    enum irc_msg_type msg_type;
    char nick[IRC_NICK_LEN];
    char hostmask[100+1];
    char modes[20+1];
};
struct irc_msg_err {
    enum irc_msg_type msg_type;
    char msg[IRC_MSG_LEN];
};
struct irc_privmsg {
    enum irc_msg_type msg_type;
    char msg[IRC_MSG_LEN];
    char from[IRC_NICK_LEN];
    // will be blank if msg_type is set to IMSG_PM
    char channel[IRC_CHAN_LEN];
};
short irc_connect(struct irc_ctx* ictx);
void irc_send_user_nick_msg(struct net_sock* ns, struct config_irc* irc_cfg);
void irc_start_auth(struct net_sock* ns, struct config_irc* irc_cfg);
void irc_send_pong(struct net_sock* ns, struct irc_msg_ping* ping_msg);
struct irc_msg* irc_get_msg(struct net_sock* ns, struct config_irc* irc_cfg);
void irc_join_chan(struct net_sock* ns, const char* channel);
void irc_send_msg(struct net_sock* ns, const char* to, const char* msg);
void irc_send_notice(struct net_sock* ns, const char* to, const char* msg);
void irc_send_who(struct net_sock* ns, const char* to);
struct irc_privmsg* _new_irc_privmsg();
void irc_read_cb(int wid, int fd, int evs, struct irc_ctx* ictx);
void irc_err_cb(int wid, int fd, int evs, struct irc_ctx* ictx);
void irc_reg_cb(int timer, struct irc_ctx* ictx);
#endif
