#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/socket.h>
#include <string.h>
#include <unistd.h>
#include <libvirt/libvirt.h>
#include <sqlite3.h>
#include <libcutil/table.h>
#include <pthread.h>
#include <signal.h>
#include "irc.h"
#include "vnc.h"
#include "net.h"
#include "msg_handler.h"
#include "virt.h"
#include "ip.h"
#include "db.h"
#include "logger.h"
#include "config.h"

struct virt_evcb_ctx evctx;

void _ca_sigcb(int c) {
    // libvirt weirdness..
    if(c != SIGPIPE && c != 17)
        evctx.running = 0;
}

void _register_sigcbs() {
    const int sigs[] = {
        SIGINT, SIGQUIT, SIGTERM, SIGHUP, SIGCHLD, SIGUSR1, SIGUSR2, SIGPIPE
    };
    struct sigaction act = {0};
    act.sa_handler = _ca_sigcb;
    for(int i = 0; i < sizeof(sigs)/sizeof(sigs[0]); i++)
        sigaction(sigs[i], &act, NULL);
}

int main(int argc, char** argv) {
    int i = -1;
    int virt_pipe[2];
    pthread_t wvirt_tid;
    struct virt_conns_ctx* hyp_ctx = NULL;
    struct vnc_ctx* vctx = NULL;
    struct irc_ctx* ictx = NULL;
    xmlDocPtr vm_template = NULL;
    sqlite3* dbc = NULL;
    _register_sigcbs();
    config_read();
    logger_init(CONFIG->logger.dst_filepath, CONFIG->logger.log_stdout);
    if(!(dbc = db_open()))
        return 1;
    xmlInitParser();
    if(!(vm_template = virt_open_template("vm_template.xml")))
        return 1;
    virInitialize();
    evctx.running = 1;
    if(!(hyp_ctx = virt_open_hyp_conns(CONFIG->hypervisor))) {
        logger_log(LOG_LEVEL_ERR, "virt", "failed to connect to hypervisors");
        return 1;
    }
    if(!(vctx = vnc_new_ctx()))
        return 1;
    vnc_connect(vctx);
    pipe(virt_pipe);
    if(pthread_create(&wvirt_tid, NULL, (void*)virt_thread_worker, &virt_pipe[0]) < 0)
        return 1;

    gnutls_global_init();
    gnutls_global_set_log_level(50);
    ictx = malloc(sizeof(struct irc_ctx) * CONFIG->irc->len);
    for(i = 0; i < CONFIG->irc->len; i++) {
        ictx[i].services_cbs = table_new();
        ictx[i].dbc = dbc;
        ictx[i].pending_deployments = table_new();
        ictx[i].irc_cfg = table_lookup_index(CONFIG->irc, i);
        ictx[i].ns = NULL, ictx[i].sctx = NULL;
        ictx[i].hyp_conns = hyp_ctx->hyp_conns;
        ictx[i].vm_template = vm_template;
        ictx[i].virt_pipe = virt_pipe[1];
        ictx[i].vctx = vctx;
        irc_connect(&ictx[i]);
    }
    while(evctx.running) {
        if(virEventRunDefaultImpl() < 0)
            break;
    }
    for(i = 0; i < CONFIG->irc->len; i++) {
        table_free(ictx[i].pending_deployments, 0);
        if(ictx[i].wevs.conn_id != -1)
            virEventRemoveTimeout(ictx[i].wevs.conn_id);
        if(ictx[i].ns != NULL)
            net_sock_close(ictx[i].ns);
        if(ictx[i].sctx != NULL)
            free(ictx[i].sctx);
        table_free(ictx[i].services_cbs, 0);
    }
    gnutls_global_deinit();
    free(ictx);
    vnc_free_ctx(vctx);
    xmlFreeDoc(vm_template);
    sqlite3_close(dbc);
    virt_cleanup(hyp_ctx);
    logger_log(LOG_LEVEL_INFO, "bot", "shutting down as per signal received");
    logger_free();
    config_free();
    close(virt_pipe[1]);
    pthread_join(wvirt_tid, NULL);
    return 0;
}
