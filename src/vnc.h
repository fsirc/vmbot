#include <stdint.h>
#include "net.h"
#ifndef H_VNC
#define H_VNC
struct vnc_ctx {
    short is_authed;
    struct net_sock* ns;
};
struct vnc_ctx* vnc_new_ctx();
void vnc_free_ctx(struct vnc_ctx* ctx);
void vnc_connect(struct vnc_ctx* ctx);
short vnc_send_host_msg(struct vnc_ctx* ctx, char* host, char* pass, uint16_t port);
void vnc_read_cb(int wid, int fd, int ev, struct vnc_ctx* ctx);
#endif
