#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>
#include <time.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include "logger.h"

logger_t* LOGGER;
static char* logger_levels[] = {"info", "error", "debug"};


// If filename == NULL, print to stdout
void logger_init(char* filename, short print_stdout) {
    assert(LOGGER == NULL);
    if(!(LOGGER = malloc(sizeof(logger_t)))) {
        fprintf(stderr, "[logger] failed to initialise\n");
        exit(1);
    }
    LOGGER->filename = filename, LOGGER->print_stdout = print_stdout;
}

void logger_log(enum logger_level level, const char* root, 
        const char* format, ...) {
    int fd = -1;
    time_t cur_time = time(NULL);
    struct tm* time_info = NULL;
    char fmt[1024] = {0}, fstr[1024] = {0}, dstr[200+1] = {0};
    va_list args;
    if(LOGGER->filename != NULL) {
        fd = open(LOGGER->filename, O_APPEND|O_WRONLY|O_CREAT, S_IRUSR|S_IWUSR|S_IRGRP);
        if(fd == -1) {
            fprintf(stderr, "[logger] failed to open log file: %s\n", LOGGER->filename);
            return;
        }
    }
    va_start(args, format);
    time_info = gmtime(&cur_time); 
    if(strftime(dstr, sizeof(dstr), "%T %F", time_info) < 1) {
        fprintf(stderr, "[logger] failed to format the date\n");
        return;
    }
    snprintf(fmt, 1024, "%s [%s][%s] %s\n", dstr, logger_levels[level], root, format);
    vsnprintf(fstr, 1024, fmt, args);
    if(!LOGGER->filename || LOGGER->print_stdout)
        printf("%s", fstr);
    if(LOGGER->filename)
        write(fd, fstr, strlen(fstr));
    va_end(args);
    if(fd != -1)
        close(fd);
}

void logger_free() {
    assert(LOGGER != NULL);
    free(LOGGER);
}
