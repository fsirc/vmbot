#include "virt.h"
#include "qtask.h"

static void free_msgh_ctx(struct msgh_cb_ctx* ctx) {
    free(ctx->from_nick);
    free(ctx->channel);
    free(ctx->network);
    free(ctx);
}

void qtask_cb_virt_gerr(void* c, struct msgh_cb_ctx* ctx) {
    char buf[IRC_MSG_LEN];
    snprintf(buf, sizeof(buf), "%s: an error occurred, contact an admin", ctx->from_nick);
    irc_send_msg(ctx->ns, ctx->channel, buf);
    free_msgh_ctx(ctx);
}

void qtask_cb_virt_ss_ok(void* c, struct msgh_cb_ctx* ctx) {
    char buf[IRC_MSG_LEN];
    snprintf(buf, sizeof(buf), "%s: VM power action completed successfully", ctx->from_nick);
    irc_send_msg(ctx->ns, ctx->channel, buf);
    free_msgh_ctx(ctx);
}

void qtask_cb_virt_destroy(void* c, struct msgh_cb_ctx* ctx) {
    char buf[IRC_MSG_LEN];
    snprintf(buf, sizeof(buf), "%s: VM destroyed", ctx->from_nick);
    irc_send_msg(ctx->ns, ctx->channel, buf);
    free_msgh_ctx(ctx);
}

void qtask_cb_vnc_ok(void* c, struct msgh_cb_ctx* ctx) {
    char buf[IRC_MSG_LEN];
    struct virt_vnc_info* vi = c;
    snprintf(buf, sizeof(buf), "%s: VNC address is %s:%u, password is %s",
             ctx->from_nick, CONFIG->vnc_gw.external_addr, CONFIG->vnc_gw.external_port, vi->pass);
    irc_send_msg(ctx->ns, ctx->from_nick, buf);
    free_msgh_ctx(ctx);
}
