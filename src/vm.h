/* vm.c is related to VM stuff only, libvirt/hypervisor daemon stuff goes into virt.c */
#ifndef H_VM
#define H_VM
#define VM_RNAME_LEN 10
#define VM_NAME_PREFIX "nvm_"
#define VM_NAME_LEN sizeof(VM_NAME_PREFIX)+VM_RNAME_LEN+1
#define VM_PASSWORD_LEN 50
short vm_new_name(char* buf);
#endif
