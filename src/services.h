#ifndef H_SERVICES
#define H_SERVICES
enum serv_type {
    SERV_BACKEND_ANOPE = 0,
    SERV_BACKEND_ATHEME = 1
};
struct serv_ctx {
    enum serv_type stype;
    short got_ctcp;
    char nick[30+1];
};
struct serv_status_resp {
    char nick[30+1];
    short registered;
};
void serv_init_conn(struct serv_ctx* ctx, struct net_sock* ns);
char* serv_build_info_cmd(const struct serv_ctx* ctx, const char* nick);
struct serv_ctx* serv_new_ctx(const char* serv_nick);
struct serv_status_resp* serv_handle_notice(struct serv_ctx* ctx,
        const char* from, 
        const char* msg);
#endif
