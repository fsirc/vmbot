#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <errno.h>
#include <poll.h>
#include <netdb.h>
#include <errno.h>
#include <libvirt/libvirt.h>
#include <time.h>
#include <unistd.h>
#include "irc.h"
#include "net.h"
#include "util.h"
#include "services.h"
#include "msg_handler.h"
#include "logger.h"
#include "config.h"

struct irc_msg* irc_get_msg(struct net_sock* ns, struct config_irc* irc_cfg) {
    struct irc_msg* imsg = NULL;
    // As per RFC 1459
    char str_msg[IRC_MSG_LEN], tmp_msg[IRC_MSG_LEN];
    array_t* sp = NULL;
    char c;
    int i = 0, d = -1;
    for(;;) {
        if((d = net_sock_recv(ns, &c, 1)) < 1) {
            if(errno == EAGAIN)
                return NULL;
            imsg = malloc(sizeof(struct irc_msg));
            imsg->msg_type = IMSG_EOF;
            return imsg;
        }
        if(i >= IRC_MSG_LEN-1 || c == '\n') {
            str_msg[i] = '\0';
            break;
        }
        str_msg[i] = c;
        i++;
    }
    strncpy(tmp_msg, str_msg, sizeof(tmp_msg));
    sp = util_split_str(tmp_msg, " ");
    if(sp->len >= 2 && !strcmp(sp->items[1], "376")) {
        imsg = malloc(sizeof(struct irc_msg));
        imsg->msg_type = IMSG_MOTD_END;
    } else if(sp->len >= 2 && !strcmp(sp->items[1], "432") &&
              strstr(str_msg+1, ":")) {
        imsg = malloc(sizeof(struct irc_msg_err));
        imsg->msg_type = IMSG_SERR;
        strncpy(((struct irc_msg_err*)imsg)->msg, strstr(str_msg+1, ":")+1,
                strlen(strstr(str_msg+1, ":")+1)-1);
    } else if(sp->len >= 8 && !strcmp(sp->items[1], "352")) {
        // WHO
        imsg = malloc(sizeof(struct irc_msg_who));
        imsg->msg_type = IMSG_WHO;
        strncpy(((struct irc_msg_who*)imsg)->hostmask, sp->items[5],
                sizeof(((struct irc_msg_who*)imsg)->hostmask));
        strncpy(((struct irc_msg_who*)imsg)->nick, sp->items[7],
                sizeof(((struct irc_msg_who*)imsg)->nick));
        strncpy(((struct irc_msg_who*)imsg)->modes, sp->items[8],
                sizeof(((struct irc_msg_who*)imsg)->modes));
    } else if(sp->len >= 3 && !strcmp(sp->items[1], "MODE")) {
        imsg = malloc(sizeof(struct irc_msg_mode));
        imsg->msg_type = IMSG_MODE;
        strncpy(((struct irc_msg_mode*)imsg)->target, sp->items[2],
                sizeof(((struct irc_msg_mode*)imsg)->target));
        strncpy(((struct irc_msg_mode*)imsg)->modes, sp->items[3]+1, 51);
    } else if(sp->len >= 1 && !strcmp(sp->items[0], "PING")) {
        imsg = malloc(sizeof(struct irc_msg_ping));
        imsg->msg_type = IMSG_PING;
        strncpy(((struct irc_msg_ping*)imsg)->msg, sp->items[1]+1,
                sizeof(((struct irc_msg_ping*)imsg)->msg));
    } else if(sp->len >= 1 && !strcmp(sp->items[0], "NICK")) {
        imsg = malloc(sizeof(struct irc_msg_nick));
        imsg->msg_type = IMSG_NICK;
        strncpy(((struct irc_msg_nick*)imsg)->nick, sp->items[1], 30);
    } else if(sp->len >= 2 && (!strcmp(sp->items[1], "PRIVMSG") || !strcmp(sp->items[1], "NOTICE"))) {
        imsg = (struct irc_msg*)_new_irc_privmsg();
        if(!strcmp(sp->items[1], "NOTICE"))
            imsg->msg_type = IMSG_NOTICE;
        if(!strstr(sp->items[0]+1, "!"))
            goto err;
        strncpy(((struct irc_privmsg*)imsg)->from, sp->items[0]+1,
                strlen(sp->items[0]+1)-strlen(strstr(sp->items[0]+1, "!")));
        if(!strcmp(sp->items[1], "PRIVMSG") &&
                strcmp(sp->items[2], irc_cfg->nick)) {
            imsg->msg_type = IMSG_CHANMSG;
            strncpy(((struct irc_privmsg*)imsg)->channel, sp->items[2], IRC_CHAN_LEN);
        }
        if(!strstr(str_msg+1, ":"))
            goto err;
        strncpy(((struct irc_privmsg*)imsg)->msg, strrchr(str_msg+1, ':')+1, IRC_MSG_LEN);
        ((struct irc_privmsg*)imsg)->msg[strlen(strrchr(str_msg+1, ':')+2)] = '\0';
    } else if(sp->len >= 1 && !strcmp(sp->items[0], "ERROR")) {
        imsg = malloc(sizeof(struct irc_msg));
        imsg->msg_type = IMSG_EOF;
    }
    array_free(sp, 0);
    return imsg;
err:
    if(imsg) free(imsg);
    array_free(sp, 0);
    return NULL;
}

static void irc_del_cbs(struct irc_ctx* ictx) {
    virEventRemoveTimeout(ictx->wevs.reg_id);
    virEventRemoveHandle(ictx->wevs.read_id);
}

static void irc_delay_connect_cb(int timer, struct irc_ctx* ictx) {
    irc_connect(ictx);
    virEventRemoveTimeout(timer);
}

short irc_connect(struct irc_ctx* ictx) {
    struct net_sock* ns = NULL;
    short flags = NETO_NONBLOCK;
    ictx->start_time = time(NULL);
    ictx->wevs.conn_id = -1;
    logger_log(LOG_LEVEL_INFO, "irc", "connecting to %s:%s:%u",
               ictx->irc_cfg->name, ictx->irc_cfg->host, ictx->irc_cfg->port);
    if(ictx->irc_cfg->tls_enabled)
        flags |= NETO_TLS_ENABLE;
    ns = net_sock_connect(ictx->irc_cfg->host, ictx->irc_cfg->port, flags);
    if(ictx->ns) {
        // free existing socket
        net_sock_close(ictx->ns);
        ictx->ns = NULL;
    }
    if(!ns) {
        // failed to connect
        logger_log(LOG_LEVEL_ERR, "irc", "%s: failed to connect to server", ictx->irc_cfg->name);
        ictx->wevs.conn_id = virEventAddTimeout(1000, (void*)irc_delay_connect_cb, ictx, NULL);
        return 0;
    }
    ictx->ns = ns;
    if((ictx->wevs.reg_id = virEventAddTimeout(2000, (void*)irc_reg_cb, ictx, NULL)) < 0)
        return 0;
    if((ictx->wevs.read_id = virEventAddHandle(ns->fd, VIR_EVENT_HANDLE_READABLE, (virEventHandleCallback)irc_read_cb, ictx, NULL)) < 0)
        return 0;
    return 1;
}

void irc_reg_cb(int timer, struct irc_ctx* ictx) {
    if(time(NULL)-ictx->start_time >= 10) {
        logger_log(LOG_LEVEL_ERR, "irc", "%s: registration timed out after 10s", ictx->irc_cfg->name);
        irc_del_cbs(ictx);
        virEventAddTimeout(1000, (void*)irc_delay_connect_cb, ictx, NULL);
        return;
    }
    irc_send_user_nick_msg(ictx->ns, ictx->irc_cfg);
}

static void irc_join_chans(struct irc_ctx* ictx) {
    int i = -1;
    for(i = 0; i < ictx->irc_cfg->channels->len; i++)
        irc_join_chan(ictx->ns, ictx->irc_cfg->channels->items[i]);
}

static short irc_is_reg_flag(struct irc_ctx* ctx,
                             struct irc_msg_mode* mode_msg) {
    if(strcmp(mode_msg->target, ctx->irc_cfg->nick) != 0)
        return 0;
    // not sure about +r, but sure
    return (strstr(mode_msg->modes, "+R") || strstr(mode_msg->modes, "+r")) ? 1 : 0;
}

// libvirt event loop cb
void irc_read_cb(int wid, int fd, int evs, struct irc_ctx* ictx) {
    struct irc_msg* imsg = NULL;
    struct serv_status_resp *sp = NULL;
    struct msgh_cb_sctx* cb_sctx = NULL;
    if(!(imsg = irc_get_msg(ictx->ns, ictx->irc_cfg)))
        return;
    if(imsg->msg_type == IMSG_MOTD_END) {
        virEventRemoveTimeout(ictx->wevs.reg_id);
        logger_log(LOG_LEVEL_INFO, "irc", "%s: registered & joining channels", ictx->irc_cfg->name);
        ictx->registered = 1;
        ictx->sctx = serv_new_ctx(ictx->irc_cfg->services_nick);
        irc_start_auth(ictx->ns, ictx->irc_cfg);
        serv_init_conn(ictx->sctx, ictx->ns);
        if(!ictx->irc_cfg->auth.wait)
            irc_join_chans(ictx);
        if(ictx->irc_cfg->on_connect_cmds)
            net_sock_write(ictx->ns, ictx->irc_cfg->on_connect_cmds,
                           strlen(ictx->irc_cfg->on_connect_cmds));
    } else if(imsg->msg_type == IMSG_PING) {
        irc_send_pong(ictx->ns, ((struct irc_msg_ping*)imsg));
    } else if(imsg->msg_type == IMSG_CHANMSG) {
        msgh_handle((void*)ictx, (struct irc_privmsg*)imsg);
    } else if(imsg->msg_type == IMSG_MODE &&
              irc_is_reg_flag(ictx, (void*)imsg) && ictx->irc_cfg->auth.wait) {
        irc_join_chans(ictx);
    } else if(imsg->msg_type == IMSG_NOTICE) {
        sp = serv_handle_notice(ictx->sctx, ((struct irc_privmsg*)imsg)->from,
                                ((struct irc_privmsg*)imsg)->msg);
        if(sp && (cb_sctx = table_lookup_key(ictx->services_cbs, sp->nick))) {
            if(sp->registered)
                cb_sctx->ok_cb(cb_sctx->ctx, &cb_sctx->im);
            else
                cb_sctx->err_cb(cb_sctx->ctx, &cb_sctx->im);
            table_del(ictx->services_cbs, sp->nick);
            free(sp);
            free(cb_sctx);
        }
    } else if(imsg->msg_type == IMSG_EOF || imsg->msg_type == IMSG_SERR) {
        if(imsg->msg_type == IMSG_EOF)
            logger_log(LOG_LEVEL_ERR, "irc", "%s: disconnected from server", ictx->irc_cfg->name);
        else
            logger_log(LOG_LEVEL_ERR, "irc", "%s: disconnected from server: %s", ictx->irc_cfg->name, ((struct irc_msg_err*)imsg)->msg);
        virEventAddTimeout(1000, (void*)irc_delay_connect_cb, ictx, NULL);
        irc_del_cbs(ictx);
    }
    free(imsg);
}

void irc_err_cb(int wid, int fd, int evs, struct irc_ctx* ictx) {
    logger_log(LOG_LEVEL_ERR, "irc", "%s: disconnected from server", ictx->irc_cfg->name);
    irc_del_cbs(ictx);
    virEventAddTimeout(1000, (void*)irc_delay_connect_cb, ictx, NULL);
}

void irc_start_auth(struct net_sock* ns, struct config_irc* irc_cfg) {
    switch(irc_cfg->auth.method) {
    case AUTH_METHOD_NICKSERV:
        net_sock_printf(ns, "PRIVMSG %s :identify %s\r\n", irc_cfg->services_nick, irc_cfg->auth.password);
        break;
    }
}

void irc_send_user_nick_msg(struct net_sock* ns, struct config_irc* irc_cfg) {
    net_sock_printf(ns, "USER %s %s %s :%s\r\n", irc_cfg->username, irc_cfg->host, irc_cfg->username,
                    irc_cfg->realname);
    if(strlen(irc_cfg->pass) >= 1) {
        net_sock_printf(ns, "PASS %s\r\n", irc_cfg->pass);
    }
    net_sock_printf(ns, "NICK %s\r\n", irc_cfg->nick);
}

void irc_join_chan(struct net_sock* ns, const char* channel) {
    net_sock_printf(ns, "JOIN %s\r\n", channel);
}

void irc_send_pong(struct net_sock* ns, struct irc_msg_ping* ping_msg) {
    net_sock_printf(ns, "PONG :%s\r\n", ping_msg->msg);
}

void irc_send_msg(struct net_sock* ns, const char* to, const char* msg) {
    net_sock_printf(ns, "PRIVMSG %s :%s\r\n", to, msg);
}

void irc_send_notice(struct net_sock* ns, const char* to, const char* msg) {
    net_sock_printf(ns, "NOTICE %s :%s\r\n", to, msg);
}

void irc_send_who(struct net_sock* ns, const char* to) {
    net_sock_printf(ns, "WHO %s\r\n", to);
}

struct irc_privmsg* _new_irc_privmsg() {
    struct irc_privmsg* s = malloc(sizeof(struct irc_privmsg));
    memset(s, 0, sizeof(struct irc_privmsg));
    s->msg_type = IMSG_PM;
    return s;
}
