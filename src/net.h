#include <stdint.h>
#include <gnutls/gnutls.h>
#ifndef H_NET
#define H_NET
struct net_sock {
    int fd;
    short tls_enabled;
    gnutls_session_t tls_ses;
    gnutls_certificate_credentials_t tls_xcred;
};
enum {
    NETO_NONBLOCK = 1,
    NETO_TLS_ENABLE = 2,
    NETO_TLS_VERIFY = 3
};
struct net_sock* net_sock_connect(const char* hostname, uint16_t port, int net_opts);
int net_sock_write(struct net_sock* ns, void* data, int len);
int net_sock_printf(struct net_sock* ns, char* fmt, ...);
int net_sock_recv(struct net_sock* ns, void* data, int len);
void net_sock_close(struct net_sock* ns);
#endif
