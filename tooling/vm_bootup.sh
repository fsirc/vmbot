#!/bin/bash

rm /etc/ssh/*host*; ssh-keygen -A

while true; do
    gw_ip=$(ip route show default | grep -E -o '([0-9]{1,3}[\.]){3}[0-9]{1,3}' | head -n 1)
    [[ $? -ne 0 ]] && $(sleep 5 && continue)
    cfg_str=$(curl -s -m 300 -A "SetupHelper" "http://$gw_ip:30647/")
    [[ $? -ne 0 ]] && $(sleep 5 && continue)
    admin_port=$(echo "$cfg_str" | awk '{split($0,c,":");print c[1]}')
    root_password=$(echo "$cfg_str" | awk '{split($0,c,":");print c[2]}')
    echo "Received password and admin port from hypervisor service: $admin_port:$root_password"
    echo "root:$root_password" | chpasswd
    echo "Port $admin_port" >> /etc/ssh/sshd_config
    $(service ssh restart || service sshd restart || /etc/init.d/sshd restart) 2>/dev/null >/dev/null
    break
done

> $0
