#!/bin/sh

rm /etc/ssh/*host*; ssh-keygen -A

FTPUSERAGENT="SetupHelper"

while true; do
    gw_ip=$(route show 0.0.0.0/0 | grep gateway | awk '{print $2}')
    $(test -z "$gw_ip" || test $0 -gt 0) && $(sleep 5 && continue)
    cfg_str=$(ftp -q 300 -o - "http://$gw_ip:30647/" 2>/dev/null)
    test -z "$cfg_str" && $(sleep 5; continue)
    admin_port=$(echo $cfg_str | awk '{split($0,c,":");print c[1]}')
    root_password=$(echo $cfg_str | awk '{split($0,c,":");print c[2]}')
    echo "Received password and admin port from hypervisor service: $admin_port:$root_password"
    if grep -q "FreeBSD" /etc/os-release; then
        echo "$root_password" | pw user mod root -h 0
    fi
    echo "Port $admin_port" >> /etc/ssh/sshd_config
    service sshd restart 2>/dev/null >/dev/null
    break
done

> $0
