import ipaddress
import sys

if len(sys.argv) <= 2:
    print("Usage: [{}] [ip_range] [ln_addr]".format(sys.argv[0]))
    exit(1)


ip_range = list(ipaddress.IPv4Network(sys.argv[1]).hosts())[1:-1]
PORT_CNT = 31
BASE_PORT = 2000
BASE_CMDS = """
iptables -t nat -A PREROUTING -d {}/32 -p tcp -m multiport --dports {}:{} \
-j DNAT --to-destination {}
iptables -t nat -A PREROUTING -d {}/32 -p udp -m multiport --dports {}:{} \
-j DNAT --to-destination {}
"""

for i, addr in enumerate(ip_range):
    cmds = "# {}/32".format(str(addr))
    port_end = BASE_PORT+(PORT_CNT*(i+1))
    port_start = (port_end-PORT_CNT)+1
    cmds += BASE_CMDS.format(sys.argv[2], port_start,
                             port_end, str(addr), sys.argv[2], port_start,
                             port_end, str(addr))
    print(cmds)
