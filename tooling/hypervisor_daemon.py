#!/usr/bin/python3
"""
Yes, this code isn't exactly efficient, but are we handling 1k sockets/minute?
No, it doesn't need to be ultra-efficient.
"""
import os
import struct
import threading
import socket
import ipaddress
import logging

PMSG_ASSIGN_DHCP_LEASE = 0
PMSG_UNASSIGN_DHCP_LEASE = 1
PMSG_VM_PASSWORD_SET = 2

LEASES_FILE_PATH = "/var/lib/misc/dnsmasq.leases"
DHCP_HOSTS_FILE_PATH = "/etc/dhcp_hosts"

vm_configurations = {}
logging.basicConfig(level=logging.INFO)


def create_ln(port: int):
    ln = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    ln.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    ln.bind(("0.0.0.0", port))
    ln.listen(256)
    return ln


http_ln, ln = create_ln(30647), create_ln(30646)


def hosts_add_entry(mac_addr: str, ip_addr: str):
    with open(DHCP_HOSTS_FILE_PATH, "r") as hosts_file:
        for line in [i.strip().split(",") for i in hosts_file.readlines()]:
            if line[0] == mac_addr:
                return
    # alright, not in the file, append it
    with open(DHCP_HOSTS_FILE_PATH, "a") as hosts_file:
        hosts_file.write("{},id:*,{}\r\n".format(mac_addr, ip_addr))


def dhcp_leases_del_entry(ip_addr: str):
    lines = []
    with open(LEASES_FILE_PATH, "r") as leases_file:
        for line in [i.strip().split(" ") for i in leases_file.readlines()]:
            if len(line) < 4 or line[2] == ip_addr:
                continue
            lines.append(" ".join(line) + "\r\n")
    with open(LEASES_FILE_PATH, "w") as leases_file:
        leases_file.writelines(lines)


def hosts_del_entry(mac_addr: str):
    hosts_entries = []
    with open(DHCP_HOSTS_FILE_PATH, "r") as hosts_file:
        for line in [i.strip() for i in hosts_file.readlines()]:
            if line.split(",")[0] != mac_addr:
                hosts_entries.append(line + "\r\n")
    with open(DHCP_HOSTS_FILE_PATH, "w") as hosts_file:
        hosts_file.writelines(hosts_entries)

# The protocol is rather simple, where messages will always be at a fixed size of 1KiB


def handle_new_sock(sock: socket.socket):
    while True:
        buf = sock.recv(1024)
        if len(buf) < 1:
            break
        if buf[0] in [PMSG_ASSIGN_DHCP_LEASE, PMSG_UNASSIGN_DHCP_LEASE]:
            # MAC_ADDR_STR | IP_ADDR_LEN | IP_ADDR
            if len(buf) < 18 or buf[18] >= len(buf)-18:
                break
            mac_addr, ip_addr = None, None
            try:
                mac_addr = buf[1:18].decode("ascii")
                if buf[0] == PMSG_ASSIGN_DHCP_LEASE:
                    ip_addr = ipaddress.IPv4Address(
                        buf[19:19+buf[18]].decode("ascii"))
            except (UnicodeDecodeError, ipaddress.AddressValueError):
                break
            if mac_addr.count(":") != 5:
                break
            if buf[0] == PMSG_ASSIGN_DHCP_LEASE:
                os.system("service dnsmasq stop 2>/dev/null>/dev/null")
                dhcp_leases_del_entry(str(ip_addr))
                hosts_add_entry(mac_addr, ip_addr)
            else:
                hosts_del_entry(mac_addr)
            os.system("service dnsmasq restart 2>/dev/null>/dev/null")
        elif buf[0] == PMSG_VM_PASSWORD_SET:
            if len(buf) < 1 or buf[1] >= len(buf)-2:
                break
            ip_addr, password = None, None
            try:
                ip_addr = ipaddress.IPv4Address(
                    buf[2:2+buf[1]].decode("ascii"))
                password = buf[3+buf[1]:3+buf[1]+buf[2+buf[1]]].decode("ascii")
                o = 3+buf[1]+buf[2+buf[1]]
                if len(buf) < o+2:
                    break
                admin_port = struct.unpack(
                    "!H", buf[o:o+2])[0]
            except (UnicodeDecodeError,
                    struct.error, ipaddress.AddressValueError,
                    IndexError):
                break
            vm_configurations[str(ip_addr)] = (admin_port, password)


def handle_http_sock(sock: socket.socket):
    global vm_configurations
    src_ip, _ = sock.getpeername()
    is_localhost = src_ip in ["127.0.0.1", "::1"]
    if not is_localhost:
        logging.info("new HTTP request from VM '{}'".format(src_ip))
    else:
        logging.info("new HTTP request from localhost, serving debug content")
    if is_localhost:
        # yes, this is perfectly valid! :D
        resp = "debug:" + str(vm_configurations)
        sock.sendall("HTTP/1.0 200 OK\r\nContent-Type: text/plain\r\nContent-Length: {}\r\n\r\n{}".format(
            len(resp), resp
        ).encode("ascii"))
    elif src_ip in vm_configurations:
        cfg_str = "{}:{}".format(
            vm_configurations[src_ip][0], vm_configurations[src_ip][1])
        sock.sendall("HTTP/1.0 200 OK\r\nContent-Type: text/plain\r\nContent-Length: {}\r\n\r\n{}".format(
            len(cfg_str), cfg_str).encode("ascii"))
        del vm_configurations[src_ip]
    else:
        sock.sendall(
            "HTTP/1.0 404 File not found\r\nConnection: close\r\nContent-Length: 0\r\n\r\n".encode("ascii"))
    sock.close()


def http_client_loop():
    global http_ln
    while True:
        sock, _ = http_ln.accept()
        threading.Thread(target=handle_http_sock, args=(sock,)).start()


threading.Thread(target=http_client_loop).start()

while True:
    sock, _ = ln.accept()
    threading.Thread(target=handle_new_sock, args=(sock,)).start()
