SRC_DIR = src
CFLAGS += -lcutil -lpthread $(shell pkg-config --cflags --libs jansson gnutls sqlite3 libvirt libxml-2.0)
_FILES = bot.h config.h irc.h util.h ip.h db.h msg_handler.h vnc.h vm.h virt.h net.h qtask.h services.h logger.h
FILES = $(patsubst %,$(SRC_DIR)/%,$(_FILES))
OBJ_FILES = $(patsubst %.h,%.o,$(FILES))
.PHONY: clean install debug

$(SRC_DIR)/%.o: %.c $(FILES)
	$(CC) -c -o $@ $< $(CFLAGS) 

bot: $(OBJ_FILES)
	$(CC) -o $@ $^ $(CFLAGS) 
install:
	install -m 775 bot /usr/bin
clean:
	rm -f bot $(SRC_DIR)/*.o
all: bot
debug: CFLAGS += -g -DDEBUG
debug: bot
